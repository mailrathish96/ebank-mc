
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.adventnet.com/webclient/clientframework" prefix="client"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="styles.css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script src="${dmjsUrl}/framework/javascript/IncludeJS.js" type="text/javascript"></script>
<script>includeMainScripts("${dmjsUrl}");</script>
<%@ page import="com.adventnet.i18n.I18N"%>
<script src="myglobals.js" type="text/javascript"></script>

<html>
<head>
    <title>E-Banking</title>
</head>
<body>
		      


<%
    String acc_no = request.getParameter("acc_no");
    System.out.println("content "+acc_no);
    int item_no = Integer.parseInt(request.getParameter("item_no"));
    String message = request.getParameter("message");
    if(acc_no != null){

%>

<script type="text/javascript">

    (function ($) {
    $(document).ready(function() {
        item_no =<%=item_no%>;

        if(item_no == 1){
			
            /*
			debugger;
			document.getElementById("account-summary-menu").style.marginRight= "0px";
             document.getElementById("account-summary-menu").style.borderTopRightRadius= "0px";
             document.getElementById("account-summary-menu").style.borderBottomRightRadius= "0px";*/
			$("#account-summary-menu").addClass("menu-item-selected");
            $("#deposit-menu").removeClass("menu-item-selected");
            $("#withdraw-menu").removeClass("menu-item-selected");
			$("#cheque-menu").removeClass("menu-item-selected");
            $("#add-beneficiary-menu").removeClass("menu-item-selected");
            $("#transfer-menu").removeClass("menu-item-selected");
            $("#mini-statement-menu").removeClass("menu-item-selected");
            document.getElementById("content-name").textContent = "Account Summary";
            $("#account-summary").show();
            $("#deposit").hide();
            $("#withdraw").hide();
			$("#cheque").hide();
            $("#beneficiary").hide();
            $("#transfer").hide();
            $("#mini-statement").hide();
        }
        if(item_no == 2){
			
			$("#account-summary-menu").removeClass("menu-item-selected");
            $("#deposit-menu").addClass("menu-item-selected");
			$("#withdraw-menu").removeClass("menu-item-selected");
			$("#cheque-menu").removeClass("menu-item-selected");
			$("#add-beneficiary-menu").removeClass("menu-item-selected");
			$("#transfer-menu").removeClass("menu-item-selected");
			$("#mini-statement-menu").removeClass("menu-item-selected");
            document.getElementById("content-name").textContent = "Deposit";
            $("#account-summary").hide();
            $("#deposit").show();
            $("#withdraw").hide();
			$("#cheque").hide();
            $("#beneficiary").hide();
            $("#transfer").hide();
            $("#mini-statement").hide();
        }
        if(item_no == 3){
			$("#account-summary-menu").removeClass("menu-item-selected");
            $("#deposit-menu").removeClass("menu-item-selected");
            $("#withdraw-menu").addClass("menu-item-selected");
			$("#cheque-menu").removeClass("menu-item-selected");
            $("#add-beneficiary-menu").removeClass("menu-item-selected");
            $("#transfer-menu").removeClass("menu-item-selected");
            $("#mini-statement-menu").removeClass("menu-item-selected");
            document.getElementById("content-name").textContent = "Withdraw";
            $("#account-summary").hide();
            $("#deposit").hide();
            $("#withdraw").show();
			$("#cheque").hide();
            $("#beneficiary").hide();
            $("#transfer").hide();
            $("#mini-statement").hide();
        }
        if(item_no == 4){
			$("#account-summary-menu").removeClass("menu-item-selected");
            $("#deposit-menu").removeClass("menu-item-selected");
            $("#withdraw-menu").removeClass("menu-item-selected");
			$("#cheque-menu").addClass("menu-item-selected");
            $("#add-beneficiary-menu").removeClass("menu-item-selected");
            $("#transfer-menu").removeClass("menu-item-selected");
            $("#mini-statement-menu").removeClass("menu-item-selected");
            document.getElementById("content-name").textContent = "Process a Cheque";
            $("#account-summary").hide();
            $("#deposit").hide();
            $("#withdraw").hide();
			$("#cheque").show();
            $("#beneficiary").hide();
            $("#transfer").hide();
            $("#mini-statement").hide();
        }
        if(item_no == 5){
			$("#account-summary-menu").removeClass("menu-item-selected");
            $("#deposit-menu").removeClass("menu-item-selected");
            $("#withdraw-menu").removeClass("menu-item-selected");
			$("#cheque-menu").removeClass("menu-item-selected");
            $("#add-beneficiary-menu").addClass("menu-item-selected");
            $("#transfer-menu").removeClass("menu-item-selected");
            $("#mini-statement-menu").removeClass("menu-item-selected");
            document.getElementById("content-name").textContent = "Add Beneficiary";
            $("#account-summary").hide();
            $("#deposit").hide();
            $("#withdraw").hide();
			$("#cheque").hide();
            $("#beneficiary").show();
            $("#transfer").hide();
            $("#mini-statement").hide();
        }
        if(item_no == 6){
			
			$("#account-summary-menu").removeClass("menu-item-selected");
            $("#deposit-menu").removeClass("menu-item-selected");
            $("#withdraw-menu").removeClass("menu-item-selected");
			$("#cheque-menu").removeClass("menu-item-selected");
            $("#add-beneficiary-menu").removeClass("menu-item-selected");
            $("#transfer-menu").addClass("menu-item-selected");
            $("#mini-statement-menu").removeClass("menu-item-selected");
            document.getElementById("content-name").textContent = "Fund Transfer";
            $("#account-summary").hide();
            $("#deposit").hide();
            $("#withdraw").hide();
			$("#cheque").hide();
            $("#beneficiary").hide();
            $("#transfer").show();
            $("#mini-statement").hide();
        }
		if(item_no == 7){
			
			$("#account-summary-menu").removeClass("menu-item-selected");
            $("#deposit-menu").removeClass("menu-item-selected");
			$("#withdraw-menu").removeClass("menu-item-selected");
			$("#cheque-menu").removeClass("menu-item-selected");
			$("#add-beneficiary-menu").removeClass("menu-item-selected");
			$("#transfer-menu").removeClass("menu-item-selected");
			$("#mini-statement-menu").addClass("menu-item-selected");
            document.getElementById("content-name").textContent = "Transaction history";
            $("#account-summary").hide();
            $("#deposit").hide();
            $("#withdraw").hide();
			$("#cheque").hide();
            $("#beneficiary").hide();
            $("#transfer").hide();
            $("#mini-statement").show();
        }


        var start = 1;
        var end = 5;
        var resultset;
        var flag = 0;
        $('#transaction_type').change(function(){
            var selected = $(this).children(":selected").text();
            switch (selected) {
                case "all transactions":
                    /*$("#myform").attr('action', 'mysql.html');
                     alert("Form Action is Changed to 'mysql.html'\n Press Submit to Confirm");*/
					$(".download").show();
                    $("#all-transactions-cv").show();
					$("#withdrawanddeposit-cv").hide();
                    $("#inbank-transactions-cv").hide();
                    $("#otherbank-transactions-cv").hide();
                    $("#cheque-transactions-cv").hide();
                    settable_all_transaction(0);
                    break;
                case "cheques":
				    $(".download").hide();
                    $("#all-transactions-cv").hide();
                    $("#withdrawanddeposit-cv").hide();
                    $("#inbank-transactions-cv").hide();
                    $("#otherbank-transactions-cv").hide();
                    $("#cheque-transactions-cv").show();
                    break;
                case "withdraw and deposits":
				    $(".download").hide();
                    $("#all-transactions-cv").hide();
                    $("#withdrawanddeposit-cv").show();
                    $("#inbank-transactions-cv").hide();
                    $("#otherbank-transactions-cv").hide();
                    $("#cheque-transactions-cv").hide();
                    break;
                case "inbank transactions":
				    $(".download").hide();
                    $("#all-transactions-cv").hide();
                    $("#withdrawanddeposit-cv").hide();
                    $("#inbank-transactions-cv").show();
                    $("#otherbank-transactions-cv").hide();
                    $("#cheque-transactions-cv").hide();
                    break;
                case "other bank transactions":
				    $(".download").hide();
                    $("#all-transactions-cv").hide();
                    $("#withdrawanddeposit-cv").hide();
                    $("#inbank-transactions-cv").hide();
                    $("#otherbank-transactions-cv").show();
                    $("#cheque-transactions-cv").hide();
                    break;
                default:
                    $("#myform").attr('action', '#');
            }
        });
		function get_transaction_count(acc_no){
            $.ajax({
                type: "post",
                url: "TransactionHistoryAction.do?method=getTransactionsCount",
                data: "acc_no=<%= acc_no%>",
                success: function(msg){
					$('#transaction-count-tag').show();
					document.getElementById("transaction-count").innerHTML = "Total transactions count : "+msg;
                    }
            });
        }
		
    });
	})(jQuery);
</script>
<script type="text/javascript">
    (function ($) {
		$.ajax({
        type: "post",
        url: "GetAccountDetailsAction.do",
        data: "acc_no=<%=acc_no%>",
        success: function(msg){
			console.log(msg);
            document.getElementById("username").innerHTML = "Username : "+msg.name;
            document.getElementById("acc_no").innerHTML = "Account number: "+msg.acc_no;
            document.getElementById("balance").innerHTML = "Total available balance  ₹"+msg.balance;
            document.getElementById("type").innerHTML = msg.type+" Account";

            var name = msg.name;
            var balance = msg.balance;
            var type = msg.type;

        },
        error : function(errorThrown) {
            alert(errorThrown);
        }
    });
	})(jQuery);
</script>
<script type="text/javascript">
    (function ($) {
    $(document).ready(function() {
        $('#inbank-submit').click(function () {
            console.log("inbank submit clicked");
            if($('#inbank-accno').val()){
                if($('#inbank-accno').val() == <%= acc_no%>){
                    document.getElementById("message").style.color = "red";
                    document.getElementById("message").textContent = "You cannot add your current account as beneficiary";
                }
                else {
                    $.ajax({
                        type: "post",
                        url: "BeneficiaryAction.do?method=addInbankBenefeciary",
                        data: "acc_no=<%= acc_no%>&to_acc_no="+$('#inbank-accno').val(),
                        success: function(msg){
                            $('#inbank').hide();
							refreshSubView("InbankView");
                            document.getElementById("inbank-beneficiary-success").style.display = "inline-block";
                            document.getElementById("message").innerHTML = "Successfull";
                        },
                        error : function(errorThrown) {
                            alert(errorThrown);
                        }
                    });
                }
            }
            else{
                document.getElementById("add-beneficiary-message").style.color = "red";
                document.getElementById("add-beneficiary-message").textContent = "please enter the account number.";
            }
        });
    });
	})(jQuery);
</script>
<script type="text/javascript">
    (function ($) {
    $(document).ready(function() {
        $('#otherbank-submit').click(function () {
            if($('#otherbank-accno').val()){

                $.ajax({
                    type: "post",
                    url: "BeneficiaryAction.do?method=addOtherbankBenefeciary",
                    data: "from_acc_no=<%= acc_no%>&acc_no="+$('#otherbank-accno').val()+"&name="+$('#otherbank-name').val()+"&bank_name="
                    +$('#otherbank-bankname').val()+"&branch_name="+$('#otherbank-branch').val()+"&ifsc="+$('#otherbank-ifsc').val(),
                    success: function(msg){
                        $('#otherbank').hide();
						refreshSubView("OtherbankView");
                        document.getElementById("otherbank-beneficiary-success").style.display = "inline-block";
                    },
                    error : function(errorThrown) {
                        alert(errorThrown);
                    }
                });
            }
            else{
                document.getElementById("add-beneficiary-message").style.color = "red";
                document.getElementById("add-beneficiary-message").textContent = "please enter the account number.";
            }
        });
    });
	})(jQuery);
</script>
<script type="text/javascript">
    (function ($) {
    $(document).ready(function() {
        $('#deposit-submit').click(function () {
            console.log("deposit submit clicked");
            if($('#deposit-amount').val()){
                $.ajax({
                    type: "post",
                    url: "WithdrawAndDepositAction.do?method=deposit",
                    data: "acc_no=<%= acc_no%>&deposit_amount="+$('#deposit-amount').val(),
                    success: function(msg){
						var input = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "acc_no").val("<%=acc_no%>");
                        $('#message-card-form').append($(input));
			            var input1 = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "item_no").val("1");
						$('#message-card-form').append($(input1));
						var input2 = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "message").val($('#deposit-amount').val()+" depositted in your account");
                        $('#message-card-form').append($(input2));
			            $('#message-card-form').submit();
                        parent.content.location = "content.jsp?acc_no=<%=acc_no%>&item_no=1&message= ₹"+$('#deposit-amount').val()+" depositted in your account";
                    },
                    error : function(errorThrown) {
                        alert("errorThrown");
                    }
                });
            }
            else{
                document.getElementsByClassName("message").style.color = "red";
                document.getElementsByClassName("message").textContent = "please enter the account number.";
            }
        });
    });
	})(jQuery);
</script>
<script type="text/javascript">
    (function ($) {
    $(document).ready(function() {
        $('#withdraw-submit').click(function () {
            console.log("deposit submit clicked");
            if($('#withdraw-amount').val()){
                $.ajax({
                    type: "post",
                    url: "WithdrawAndDepositAction.do?method=withdraw",
                    data: "acc_no=<%= acc_no%>&withdraw_amount="+$('#withdraw-amount').val(),
                    success: function(msg){
						var input = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "acc_no").val("<%=acc_no%>");
                        $('#message-card-form').append($(input));
			            var input1 = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "item_no").val("1");
					    $('#message-card-form').append($(input1));
						var input2 = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "message").val($('#withdraw-amount').val()+" withdrawn from your account");
                        $('#message-card-form').append($(input2));
			            $('#message-card-form').submit();
                    },
                    error : function(errorThrown) {
                        alert("errorThrown");
                    }
                });
            }
            else{
                document.getElementsByClassName("message").style.color = "red";
                document.getElementsByClassName("message").textContent = "please enter the account number.";
            }
        });
    });
	})(jQuery);
</script>
<script type="text/javascript">
    (function ($) {
    $(document).ready(function() {
        var flag = 0;
        var beneficiary_list;
        var selected =0;
        $('#beneficiary-list').click(function(){
            if(flag == 0){
                flag = 1;
                $.ajax({
                    type: "post",
                    url: "BeneficiaryAction.do?method=getBeneficiaryList",
                    data: "acc_no=<%= acc_no%>",
                    success: function(msg){
                        beneficiary_list = msg;
                        console.log("be list "+msg);
                        var tr;
                        for (var i = 0; i < msg.length; i++) {
                            tr = $('<option/>');
                            tr.append(msg[i].to_account);
                            $('#beneficiary-list').append(tr);
                        }
                    },
                    error : function(errorThrown) {
                        alert(errorThrown);
                    }
                });
            }
        });
        $('#beneficiary-list').change(function(){
            selected = $(this).children(":selected").index();
            document.getElementById("to_account").textContent = "Selected Account  :"+beneficiary_list[selected-1].to_account;
        });
        $('#transfer-submit').click(function () {
            console.log("transfer submit clicked");
            x = beneficiary_list[selected-1].to_account;
            var to_account_no;
            if(beneficiary_list[selected-1].type == 1){
                to_account_no = x;
            }else{
                to_account_no = x.substr(0,x.indexOf(' '));
            }
            if($('#transfer-amount').val()){

                $.ajax({
                    type: "post",
                    url: "FundTransferAction.do",
                    data: "acc_no=<%= acc_no%>&transfer_amount="+$('#transfer-amount').val()+"&to_acc_no="+ to_account_no +"&pair_id="+ beneficiary_list[selected-1].pair_id+"&type="+beneficiary_list[selected-1].type,
                    success: function(msg){
						var input = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "acc_no").val("<%=acc_no%>");
                        $('#message-card-form').append($(input));
			            var input1 = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "item_no").val("1");
						$('#message-card-form').append($(input1));
						var input2 = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "message").val($('#transfer-amount').val()+" transfered to "+to_account_no);
                        $('#message-card-form').append($(input2));
			            $('#message-card-form').submit();
                    },
                    error : function(errorThrown) {
                        alert("errorThrown");
                    }
                });
            }
            else{
                document.getElementsByClassName("message").style.color = "red";
                document.getElementsByClassName("message").textContent = "please enter the account number.";
            }
        });
    });
	})(jQuery);
</script>
<script type="text/javascript">
    (function ($) {
    $(document).ready(function() {
        $('#cheque-submit').click(function () {
            console.log("cheque submit clicked");
            if($('#cheque-amount').val()){
                $.ajax({
                    type: "post",
                    url: "ChequeTransferAction.do",
                    data: "acc_no=<%= acc_no%>&payee_acc_no="+$('#payee-accno').val()+"&payee_name="+$('#payee-name').val()+"&payee_bank="+$('#payee-bank').val()+"&payee_ifsc="+$('#payee-ifsc').val()+"&cheque_amount="+$('#cheque-amount').val(),
                    success: function(msg){
						var input = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "acc_no").val("<%=acc_no%>");
                        $('#message-card-form').append($(input));
			            var input1 = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "item_no").val("1");
					    $('#message-card-form').append($(input1));
						var input2 = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "message").val($('#cheque-amount').val()+" depositted through cheque");
                        $('#message-card-form').append($(input2));
			            $('#message-card-form').submit();
                        parent.content.location = "content.jsp?acc_no=<%=acc_no%>&item_no=1&message= ₹"+$('#cheque-amount').val()+" depositted through cheque";
                    },
                    error : function(errorThrown) {
                        alert("errorThrown");
                    }
                });
            }
            else{
                document.getElementsByClassName("message").style.color = "red";
                document.getElementsByClassName("message").textContent = "please enter the account number.";
            }
        });
    });
	})(jQuery);
</script>
<script type="text/javascript">
    (function ($) {
    $(document).ready(function() {
        $('#add-inbank-ben').click(function () {
            $("#add-inbank-ben").addClass("tab-clicked");
            $("#add-otherbank-ben").removeClass("tab-clicked");
            $("#show-inbank-ben").removeClass("tab-clicked");
			$("#show-otherbank-ben").removeClass("tab-clicked");
            $("#inbank-block").show();
            $("#otherbank-block").hide();
            $("#inbank-mc-block").hide();
			$("#otherbank-mc-block").hide();
        });
		$('#add-otherbank-ben').click(function () {
            $("#add-inbank-ben").removeClass("tab-clicked");
            $("#add-otherbank-ben").addClass("tab-clicked");
            $("#show-inbank-ben").removeClass("tab-clicked");
			$("#show-otherbank-ben").removeClass("tab-clicked");
            $("#inbank-block").hide();
            $("#otherbank-block").show();
            $("#inbank-mc-block").hide();
			$("#otherbank-mc-block").hide();
        });
		$('#show-inbank-ben').click(function () {
            $("#add-inbank-ben").removeClass("tab-clicked");
            $("#add-otherbank-ben").removeClass("tab-clicked");
            $("#show-inbank-ben").addClass("tab-clicked");
			$("#show-otherbank-ben").removeClass("tab-clicked");
            $("#inbank-block").hide();
            $("#otherbank-block").hide();
            $("#inbank-mc-block").show();
			$("#otherbank-mc-block").hide();
        });
		$('#show-otherbank-ben').click(function () {
            $("#add-inbank-ben").removeClass("tab-clicked");
            $("#add-otherbank-ben").removeClass("tab-clicked");
            $("#show-inbank-ben").removeClass("tab-clicked");
			$("#show-otherbank-ben").addClass("tab-clicked");
            $("#inbank-block").hide();
            $("#otherbank-block").hide();
            $("#inbank-mc-block").hide();
			$("#otherbank-mc-block").show();
        });
    });
	})(jQuery);
</script>

<script>
    (function ($) {
    $(document).ready(function() {
        $('#PDF-submit').click(function () {
			console.log("pdf");
            var pdfurl = getViewURL('TransactionsView', 'pdf');
		    openURL(pdfurl);
        });
		$('#XLXS-submit').click(function () {
			console.log("xlxs");
            var xlsurl = getViewURL('TransactionsView', 'xlsx');
		    openURL(xlsurl);
        });
		$('#CSV-submit').click(function () {
			console.log("csv");
            var csvurl = getViewURL('TransactionsView', 'csv');
		    openURL(csvurl);
        });
    });
	})(jQuery);

function deleteSystem(id, type) {
	var params = "";
	var url = ""; 
	if(type == 1){
		params = "&acc_no=" + id;
		url = "/ebank-mc/DeleteBeneficiaryAction.do?method=deleteinbankben"; 

	}else{
		params = "&ben_id=" + id;
		url = "/ebank-mc/DeleteBeneficiaryAction.do?method=deleteotherbankben"; 
	}
	if (confirm("Are you sure you want to delete the beneficiary details?")) { 
                        var xhttp;
                        if (window.XMLHttpRequest) {
                            xhttp = new XMLHttpRequest();
                        } else {
                            xhttp = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        xhttp.open("POST", url, true);
                        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        xhttp.send(params);
                        xhttp.onreadystatechange = function () {
                            if (xhttp.readyState == 4) {
                                var response = xhttp.responseText;
                            }
                            response = String(response);
							console.log(response);
                            if (response) {
                                refreshSubView("InbankView");
								refreshSubView("OtherbankView");
                            }
                        };
                    }
	}
	
	function editSystem(id) {
                showURLInDialog("/ebank-mc/EditBeneficiary.jsp?ben_id=" +id, 'closeOnEscKey=yes,position=ABSMiddle,modal=yes,width=1000,height=200,scrollbars=no,title=Edit Detail'); //No I18N
				refreshSubView("TransactionsView");
			}
	function modifySystem(){
		console.log("modify");
		refreshSubView("TransactionsView");
		location.reload();
		location.refreshSubView("TransactionsView");
	}


</script>

<div>

    <form action="MenuLinkAction.do" id="message-card-form">
	</form>
    <div class="content-topbar">
        <span id ="content-name">Account summary</span>
    </div>
    <div id="account-summary">
        <div id="account-summary-main-area">
            <p id="username"></p>
            <p id="acc_no"></p>
            <p id="type"></p>
            <p id="balance"></p>
            <%
                if(message != null){%>
                    <div id="card">
                         <p><%=message%></p>
                    </div>
                <%
                }%>
        </div>
    </div>
    <div id="deposit">
        <div>
            <div id="deposit-form">
                <p>
                    <label class="label-text">Deposit amount</label>
                    <input type="text" class="text-box" id="deposit-amount" />
                </p>
                <p class = "deposit-message"></p>
                <input type="submit" class="submit-button" id="deposit-submit" value="Deposit" />
            </div>
            <div class="successfull-entry" id="deposit-success">
                <p class="success-message"><b>Amount depositted successfully!!!</b></p>
            </div>
        </div>
    </div>
    <div id="withdraw">
        <div>
            <div id="withdraw-form">
                <p>
                    <label class="label-text">Withdraw amount</label>
                    <input type="text" class="text-box" id="withdraw-amount" />
                </p>
                <p class = "withdraw-message"></p>
                <input type="submit" class="submit-button" id="withdraw-submit" value="Withdraw" />
            </div>
            <div class="successfull-entry" id="withdraw-success">
                <p class="success-message"><b>Amount withdrawn successfully!!!</b></p>
            </div>
        </div>
    </div>
	<div id="cheque">
        <div>
            <div id="cheque-form">
			    <p>
                    <label class="label-text">Payee Name</label>
                    <input type="text" class="text-box" id="payee-name" />
                </p>
				<p>
                    <label class="label-text">Payee Account Number</label>
                    <input type="text" class="text-box" id="payee-accno" />
                </p>
				<p>
                    <label class="label-text">Payee Bank</label>
                    <input type="text" class="text-box" id="payee-bank" />
                </p>
				<p>
                    <label class="label-text">Payee IFSC</label>
                    <input type="text" class="text-box" id="payee-ifsc" />
                </p>
                <p>
                    <label class="label-text">Cheque amount</label>
                    <input type="text" class="text-box" id="cheque-amount" />
                </p>
                <p class = "cheque-message"></p>
                <input type="submit" class="submit-button" id="cheque-submit" value="Process Cheque" />
            </div>
            <div class="successfull-entry" id="cheque-success">
                <p class="success-message"><b>Cheque processed successfully!!!</b></p>
            </div>
        </div>
    </div>
    <div id="beneficiary">
	    
        <div id="beneficiary-block">
		<center><div id="beneficiary-tab">
		      <div class="tab-menu" id="add-inbank-ben"><span>Add Inbank Beneficiary</span></div>
			  <div class="tab-menu" id="add-otherbank-ben"><span>Add Otherbank Beneficiary</span></div>
			  <div class="tab-menu" id="show-inbank-ben"><span>Show Inbank Beneficiary</span></div>
			  <div class="tab-menu" id="show-otherbank-ben"><span>Show Otherbank Beneficiary</span></div>
		</div></center>
            <div id="inbank-block">
                <div id="inbank">
                    <p>
                        <label class="label-text">Account number</label>
                        <input type ="text" class="text-box" id="inbank-accno" />
                    </p>
                    <p id = "add-beneficiary-message"></p>
                    <input type="submit" class="submit-button" id="inbank-submit" value="Add inbank beneficiary" />

                </div>
                <div class="successfull-entry" id="inbank-beneficiary-success">
                    <p class="success-message"><b>Beneficiary added successfully!!!</b></p>
                </div>
            </div>
            <div id="otherbank-block">
                <div id="otherbank">
                    <p>
                        <label class="label-text">Account number</label>
                        <input type ="text" class="text-box" id="otherbank-accno" />
                    </p>
                    <p>
                        <label class="label-text">Name</label>
                        <input type ="text" class="text-box" id="otherbank-name" />
                    </p>
                    <p>
                        <label class="label-text">Bank</label>
                        <input type ="text" class="text-box" id="otherbank-bankname" />
                    </p>
                    <p>
                        <label class="label-text">Branch</label>
                        <input type ="text" class="text-box" id="otherbank-branch" />
                    </p>
                    <p>
                        <label class="label-text">IFSC</label>
                        <input type ="text" class="text-box" id="otherbank-ifsc" />
                    </p>
                    <p id = "add-otherbank-beneficiary-message"></p>
                    <input type="submit" class="submit-button" id="otherbank-submit" value="Add Ohter bank beneficiary"/>
                </div>
                <div class="successfull-entry" id="otherbank-beneficiary-success">
                    <p class="success-message"><b>Beneficiary added successfully!!!</b></p>
                </div>
            </div>
			<div id="inbank-mc-block"><client:showView viewName="InbankView"/></div>
			<div id="otherbank-mc-block"><client:showView viewName="OtherbankView"/></div>
        </div>
    </div>
    <div id="transfer">
        <div>
            <div id="transfer-form">
                <select  id = "beneficiary-list" name="beneficiary-list">
                    <option value="select">---select---</option>
                </select>
                <p class="label-text" id="to_account"></p>
                <p>
                    <label class="label-text">Transfer amount</label>
                    <input type="text" class="text-box" id="transfer-amount" />
                </p>
                <p class = "deposit-message"></p>
                <input type="submit" class="submit-button" id="transfer-submit" value="Transfer" />
            </div>
            <div class="successfull-entry" id="transfer-success">
                <p class="success-message"><b>Amount Transfered successfully!!!</b></p>
            </div>
        </div>
    </div>
    <div id="mini-statement">
        <div id="mini-statement-table">
		    <div id="transaction-count-tag">
			<p id="transaction-count"></P>
			</div>
            <select  id = "transaction_type" name="transaction_type">
                <option value="select">---select---</option>
                <option value="all transactions">all transactions</option>
                <option value="cheques">cheques</option>
                <option value="withdraw and deposits">withdraw and deposits</option>
                <option value="inbank transactions">inbank transactions</option>
                <option value="other bank transactions">other bank transactions</option>
            </select>
			<div class="download" ><div class="buttons"><div class="download-button" id="PDF-submit"><span>PDF</span></div><div class="download-button" id="XLXS-submit"><span>XLXS</span></div><div class="download-button" id="CSV-submit"><span>CSV</span></div></div></div>
            <div id="all-transactions-cv"><client:showView viewName="TransactionsView"/></div>
			<div id="withdrawanddeposit-cv"><client:showView viewName="withdrawanddeposit"/></div>
            <div id="inbank-transactions-cv"><client:showView viewName="inbank-transactions"/></div>
            <div id="otherbank-transactions-cv"><client:showView viewName="otherbank-transactions"/></div>
            <div id="cheque-transactions-cv"><client:showView viewName="cheque-transactions"/></div>
        </div>
    </div>
	 
</div>
<%
    }
%>
</body>
</html>