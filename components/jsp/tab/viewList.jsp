<%-- $Id$ --%>
<%@ include file='../CommonIncludes.jspf'%>
<%@ page import="com.adventnet.client.components.tab.web.*" %>
<%@page import="com.adventnet.iam.xss.IAMEncoder"%>

<%
  TabModel model = (TabModel)viewContext.getViewModel();
  TabModel.TabIterator ite = model.getIterator();
  String ctxPath = ((HttpServletRequest)pageContext.getRequest()).getContextPath();
%>
<script>
function reloadData(tabName){
	updateState('<%=uniqueId%>', 'selectedView', tabName.replace(' ', '_'),true);<%--NO OUTPUTENCODING --%>
	window.location.reload();
}
</script>
<DIV id="<%=uniqueId%>_Border" class="divBorder"><%--NO OUTPUTENCODING --%>
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
			<%
			if(model.getChildConfigList().size() > 0){
			%>
				<td>
					<ul class='listViews'>
						<%
							while(ite.next()){
								%>
								<li>
								<p>
								<a href='<%=IAMEncoder.encodeHTMLAttribute(ite.getTabAction())%>'  class='<%=IAMEncoder.encodeHTMLAttribute(ite.getCurrentClass())%>'>
								<%
								if(ite.getImage() != null){
									//String imgSrc = ite.getImage();
									String imgSrc = ThemesAPI.handlePath(ite.getImage(),request,themeDir);
                					//imgSrc = (imgSrc.charAt(0) == '/')?request.getContextPath() + imgSrc:imgSrc;
									%>
									<img src='<%= imgSrc%>'/><%}%><br><%=IAMEncoder.encodeHTMLAttribute(ite.getTitle())%></a></p><%--NO OUTPUTENCODING --%>
								</li>
								<%
							}
						%>
					</ul>
				</td>
				<%
				if(model.getViewType().equals("verticaltab")){
				%>
					</tr>
					<tr>
						<td>
						<span id="MC-Customization" class="hide customize">
							&nbsp;<button onClick="javascript:window.open('TabCustomization.cc?TYPE=Navigation&VIEWNAME=<%=viewContext.getModel().getViewName()%>&UNIQUEID=<%=uniqueId%>&DefaultGroup=Reports&DisplayName=Links', null, 'height=500,width=800,scrollbars=1,resizable=1')" class="customizeButton" onMouseOver="return showBorder('<%=uniqueId%>')" onMouseOut="return hideBorder('<%=uniqueId%>')">Add / Remove Links</button><%--NO OUTPUTENCODING --%>
						</span>
						</td>
					<%
				}
				else {
					%>
					<td align="right" valign="top">
						<span id="MC-Customization" class="hide customize">
							&nbsp;<button class="customizeButtonPull" id="ICMTab<%=uniqueId%>" onClick="showCustomizeLinks('TabCustomizationLinks','ICMTab<%=uniqueId%>',event, '<%=uniqueId%>')" onMouseOver="return showBorder('<%=uniqueId%>')" onMouseOut="return hideBorder('<%=uniqueId%>')">Customize &nbsp;&nbsp;</button><%--NO OUTPUTENCODING --%>
						</span>
					</td>
					<%
				}
				%>
				<%
			}
			else {
			%>
				<%
				if(model.getViewType().equals("verticaltab")){
					%>
					<tr>
						<td align="center" valign="middle">
							<span class="subTitle">[&nbsp;<%=IAMEncoder.encodeHTML((String)viewContext.getModel().getViewConfiguration().getFirstValue("ViewConfiguration", "TITLE"))%>&nbsp;]</span> <br>
							&nbsp;<button onClick="javascript:window.open('TabCustomization.cc?TYPE=Navigation&VIEWNAME=<%=viewContext.getModel().getViewName()%>&UNIQUEID=<%=uniqueId%>&DefaultGroup=Reports&DisplayName=Links', null, 'height=500,width=800,scrollbars=1,resizable=1')" class="customizeButton" onMouseOver="return showBorder('<%=uniqueId%>')" onMouseOut="return hideBorder('<%=uniqueId%>')">Add / Remove Links</button><%--NO OUTPUTENCODING --%>
						</td>
					<%
				}
				else {
					%>
					<td align="center" valign="middle">
						<span class="subTitle">[&nbsp;<%=IAMEncoder.encodeHTML((String)viewContext.getModel().getViewConfiguration().getFirstValue("ViewConfiguration", "TITLE"))%>&nbsp;]</span> <br><%--NO OUTPUTENCODING --%>
						&nbsp;<button class="customizeButtonPull" id="ICMTab<%=uniqueId%>" onClick="showCustomizeLinks('TabCustomizationLinks','ICMTab<%=uniqueId%>',event, '<%=uniqueId%>')" onMouseOver="return showBorder('<%=uniqueId%>')" onMouseOut="return hideBorder('<%=uniqueId%>')">Customize &nbsp;&nbsp;</button><%--NO OUTPUTENCODING --%>
					</td>
				<%
				}
			}
		%>
		</tr>
	</table>
</DIV>


<DIV id = 'TabCustomizationLinks<%=uniqueId%>' class = 'customizeMenu' style='display:none;'><%--NO OUTPUTENCODING --%>
	<TABLE border = '0' cellspacing = '0' cellpadding = '0'>
		<TR onClick="openCustomizationWindow('AddTab.do?VIEWNAME=<%=viewContext.getModel().getViewName()%>', 'height=200,width=250,scrollbars=1,resizable=1,status=yes')" class = 'customizeMenuItem' onMouseOver='this.className="customizeMenuItemOver"' onMouseOut='this.className="customizeMenuItem"'><%--NO OUTPUTENCODING --%>
			<TD>Create New Tab ...</TD>
		</TR>
		<TR onClick="openCustomizationWindow('TabCustomization.cc?TYPE=Tab&VIEWNAME=<%=viewContext.getModel().getViewName()%>&DefaultGroup=Tabs', 'height=500,width=800,scrollbars=1,resizable=1')" class = 'customizeMenuItem' onMouseOver='this.className="customizeMenuItemOver"' onMouseOut='this.className="customizeMenuItem"'><%--NO OUTPUTENCODING --%>
			<TD>Modify Existing Tabs ...</TD>
		</TR>
	</TABLE>
</DIV>
