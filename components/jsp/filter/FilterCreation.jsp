<%-- $Id$ --%>
<%@ include file='/components/jsp/CommonIncludes.jspf'%>
<%@ page import="java.util.*"%>
<%@ page import="com.adventnet.persistence.Row"%>
<%@ page import="com.adventnet.i18n.*"%>
<%@ page import="com.adventnet.client.tpl.*"%>
<%@ page import="com.adventnet.iam.xss.IAMEncoder" %>

<link href='<%=themeDir%>/styles/cvcriteria.css' rel='stylesheet' type='text/css'><%--NO OUTPUTENCODING --%>

<% String templateHTML = TemplateAPI.givehtml("CriteriaTemplates", null, new Object[][]{{"contains",I18N.getMsg("contains")}, 
		{"doesn't contain", I18N.getMsg("doesn't contain")}, {"ends with", I18N.getMsg("ends with")},
		{"starts with", I18N.getMsg("starts with")}, {"is", I18N.getMsg("is")}, 
		{"isn't", I18N.getMsg("isn't")}, {"is higher than", I18N.getMsg("is higher than")}, 
		{"is lower than", I18N.getMsg("is lower than")}, {"is after", I18N.getMsg("is after")},
		{"is before", I18N.getMsg("is before")}, {"Yes", I18N.getMsg("Yes")}, {"No", I18N.getMsg("No")}, 
		{"True", I18N.getMsg("True")}, {"False", I18N.getMsg("False")},{"Context_Path",request.getContextPath()}});%>
<%=templateHTML%><%--NO OUTPUTENCODING --%>

<%Row filterRow = (Row)request.getAttribute("FILTER");  %>
<script src="<%=request.getContextPath()%>/components/javascript/Criteria.js"></script><%--NO OUTPUTENCODING --%>
<script>

<%=request.getAttribute("CRITERIA_DEFN")%><%--NO OUTPUTENCODING --%>
var crList = <%=request.getAttribute("RELCRITERIALIST")%>;<%--NO OUTPUTENCODING --%>
cr.matchVal = <%=request.getAttribute("LOGICALOP")%>;<%--NO OUTPUTENCODING --%>

</script>
<%
String row="";
Object dispalyName=(filterRow!=null)?filterRow.get("DISPLAYNAME"):"";
if(filterRow!=null)
{
row=(String)(filterRow.get("FILTERNAME"));
}
String html=com.adventnet.client.tpl.TemplateAPI.givehtml("FilterCreateForm",null,new Object[][] {{"UNIQUEID",uniqueId},
		{"VIEWNAME",request.getParameter("VIEWNAME")},{"EVENTTYPE",request.getParameter("EVENT_TYPE")},
		{"LISTID",request.getParameter("LISTID")},{"FILTERNAME",row},{"DISPLAYNAME",dispalyName}});
%>
<%=html%><%--NO OUTPUTENCODING --%>

