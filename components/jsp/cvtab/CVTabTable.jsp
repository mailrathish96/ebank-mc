<%-- $Id$ --%>
<%@ page import="com.adventnet.client.components.tab.web.*"%>
<%@ include file='/components/jsp/CommonIncludes.jspf'%>
<%@ page import="com.adventnet.iam.xss.IAMEncoder" %>

<form name="cvselform" method="post" action="<%=IAMEncoder.encodeHTMLAttribute(uniqueId)%>.ve" onSubmit="return handleStateForForm(this)">
<table>
<tr>
<td>
<%@ include file='CVCombo.jspf'%>
</td>
<td>
<% String editDelete = (String)request.getAttribute("EDITDELETE"); %>   
<input type="submit" name="EVENT_TYPE" <%=IAMEncoder.encodeHTMLAttribute(editDelete)%> value="Edit"/>
</td>
<td>
<input type="submit" name="EVENT_TYPE" <%=IAMEncoder.encodeHTMLAttribute(editDelete)%> value="Delete"/>
</td>
<td>
<input type="submit" name="EVENT_TYPE" value="Create"/>
</td>
</tr>
</table>
</form>

<client:showView viewName='<%=(String)viewContext.getStateParameter("selectedView")%>'/>
