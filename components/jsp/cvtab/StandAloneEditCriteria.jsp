<%-- $Id$ --%>
<%@ include file='/components/jsp/CommonIncludes.jspf'%>
<%@ page import="com.adventnet.iam.xss.IAMEncoder" %>
<%@ page import="java.util.*"%>
<%@ include file="CVCreationJSIncludes.jspf"%>


<link href="<%=request.getContextPath()%>/themes/common/styles/cv_creation.css" rel="stylesheet" type="text/css"><%--NO OUTPUTENCODING --%>
<form method="post" action="<%=IAMEncoder.encodeHTMLAttribute(uniqueId)%>.ve" name="CVCreationForm" onSubmit="return handleStateForForm(this);"><%--NO OUTPUTENCODING --%>
<%if(request.getParameter("VIEWNAME") != null){ %>
<input type="hidden" name="VIEWNAME" value="<%=IAMEncoder.encodeHTMLAttribute(request.getParameter("VIEWNAME"))%>"/>
<%} else {%>
<input type="hidden" name="PARENTVIEWNAME" value="<%=IAMEncoder.encodeHTMLAttribute(request.getParameter("PARENTVIEWNAME"))%>"/>
<table><tr><td>Title: <input type="text" name="NEWVIEWTITLE"/></td></tr>
<tr><td>
<%if(request.getParameter("ADDTOVIEW")!= null){%>
<input type="hidden" name="ADDTOVIEW"
value="<%=IAMEncoder.encodeHTMLAttribute(request.getParameter("ADDTOVIEW"))%>"/>
<%}%>
<%}%>
<%@ include file="SpecifyCriteria.jspf"%>
<!-- A RAD Hack -->
<script>Search.numCheck=null;</script>
</td></tr>
<tr>
<td align="center" height="30">
<input type="submit" name="Save" value="Save" class="button"/>
<input type="button" name="Cancel" value="Cancel" class="button" onClick="closeView('<%=IAMEncoder.encodeJavaScript(uniqueId)%>');"/><%--NO OUTPUTENCODING --%>
</td>
</tr>
</table>
</form>