<%-- $Id$ --%>
<%@ include file='../CommonIncludes.jspf'%>
<%@ page import="com.adventnet.client.components.tab.web.*" %>
<%@ page import="com.adventnet.persistence.*" %>
<%@ page import="com.adventnet.client.view.web.WebViewAPI"%>
<%@ page import="com.adventnet.iam.xss.IAMEncoder" %>
<%
  TabModel model = (TabModel)viewContext.getViewModel();
  TabModel.TabIterator ite = model.getIterator();
  String ctxPath = ((HttpServletRequest)pageContext.getRequest()).getContextPath();
%>
<SCRIPT>
    var <%=uniqueId%>_CHILD_CV = new Array();<%--NO OUTPUTENCODING --%>
</SCRIPT>
	                     <table  class="cvtab" cellspacing="0" cellpadding="0" border="0" width="100%" nowrap>
		 	<tr><td height="1px" width="80%" ></td><td height="1px"><div style="position:relative;top:-22;"  width="100%" ><act:ShowMenuItem menuItemId="CVMenu_Add" uniqueId='<%=IAMEncoder.encodeHTMLAttribute(uniqueId)%>'/></div></td><td height="1px"></td></tr>
				<%
					if(model.getViewType().equals("verticaltab"))
                    { 
                        int i=-1;
						
						DataObject dobj = ViewContext.getViewContext(uniqueId,request).getModel().getViewConfiguration();
                                                Long parentViewNameNo = (Long)dobj.getFirstValue("ACCVTabParentConfig","PARENTVIEWNAME");
                                                String parentViewName  =  WebViewAPI.getViewName(parentViewNameNo);
				%>
                        <SCRIPT>
                            <%=uniqueId%>_PARENT_CV = "<%=IAMEncoder.encodeJavaScript(parentViewName)%>";<%--NO OUTPUTENCODING --%>
                        </SCRIPT>
				<%
		    			while(ite.next())
						{
				%>
                        <SCRIPT>
                            <%=uniqueId%>_CHILD_CV[<%=++i%>] = "<%=IAMEncoder.encodeJavaScript(ite.getCurrentView())%>";<%--NO OUTPUTENCODING --%>
                        </SCRIPT>
                           <tr>
                              <td class='notselected' cref='<%=IAMEncoder.encodeJavaScript(ite.getCurrentRefId())%>'> 
                                <a href='<%=IAMEncoder.encodeJavaScript(ite.getTabAction())%>'>
								<%
								String icoFile = ite.getChildIconFile();
								if(icoFile != null) {
									icoFile = (icoFile.charAt(0) == '/')?ctxPath + icoFile:icoFile;
								    %>
									<img src='<%= IAMEncoder.encodeHTML(icoFile)%>'/>
									<%
								}
								%>
								<%=IAMEncoder.encodeHTML(ite.getTitle())%></a>
                          </td>
			   <act:ShowCVMenu menuId="CVUpdateMenu" uniqueId='<%=IAMEncoder.encodeHTMLAttribute(uniqueId)%>' index='<%=i+""%>'/>
                        
                          </tr>
    			<%
    					}
					}
				%>
					</table>

<script><%=model.getTabSelectionJS()%></script><%--NO OUTPUTENCODING --%>
