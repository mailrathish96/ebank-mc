<%-- $Id$ --%>

<%@ include file='/components/jsp/CommonIncludes.jspf'%>
<%@ page import="java.util.*"%>
<%@page import="com.adventnet.iam.xss.IAMEncoder"%>
<%@ include file="CVCreationJSIncludes.jspf"%>

<link href="<%=request.getContextPath()%>/themes/common/styles/cv_creation.css" rel="stylesheet" type="text/css"><%--NO OUTPUTENCODING --%>

<% 
    String selectquery_id = ((Long)request.getAttribute("SELECTQUERY_ID")).toString();
    String cvc_parent_viewName = (String)request.getParameter("CVC_PARENT_VIEWNAME");
    String cvc_current_viewName = (String)request.getParameter("CVC_CURRENT_VIEWNAME");
    String actionSource = request.getParameter("ACTION_SOURCE");
    boolean showParentView = ((Boolean)request.getAttribute("SHOW_PARENT_VIEW")).booleanValue();
    boolean isCVAddition = false;
    String page_title = null;
    
    String customViewName = null;
    String readOnly = "";

    if( showParentView )
    {
        customViewName = "";
        isCVAddition = true;
        page_title = "Create New View";
    }
    else
    {
        customViewName = IAMEncoder.encodeJavaScript(cvc_current_viewName);
        readOnly="readOnly";
        page_title = "Edit Custom View";
    }
    
%>

<title><%=page_title%></title>
</head>
<table width="95%" border="0" align="center" cellpadding="2" cellspacing="1">
  <tr>
    <td><form method="post" name="CVCreationForm" action="CVMenu_AddCVAction.ma" onSubmit="return formSubmit();">
        <input type="hidden" name="SELECTQUERY_ID" value="<%=selectquery_id%>"><%--NO OUTPUTENCODING --%>
        <input type="hidden" name="IS_CV_ADDITION" value="<%=isCVAddition%>"><%--NO OUTPUTENCODING --%>
        <input type="hidden" name="CVC_PARENT_VIEWNAME" value="<%=IAMEncoder.encodeHTMLAttribute(cvc_parent_viewName)%>">
        <%if(actionSource != null){%> <input type="hidden" name="ACTION_SOURCE" value="<%=IAMEncoder.encodeHTMLAttribute(actionSource)%>">
        <%}%>

        <table border=0 align="center" cellPadding=0 cellSpacing=0 class="secTable">
          <tr> 
            <td colspan="2" class="secHead"><b>1. View.information</b></td>
          </tr>
          <tr> 
            <td class="secContent" width="84">View Name:</td>
            <td width="394" class="secContent"> <input type="text" id="customviewname" name="customviewname" "<%=readOnly%>" value="<%=customViewName%>"
               class="textField" maxlength="50" size="50"><%--NO OUTPUTENCODING --%>
              &nbsp;&nbsp;&nbsp;<font color="#ff0000">*</font> </td>
          </tr>
        </table>
        <br>
           <%@ include file="SpecifyCriteria.jspf"%>
        <br>
        <br>
           <%@ include file="ChooseColumns.jspf"%>
        <br>
        <div id="item_movelayer" class="moveLayer"></div>
        <p></p>
        <table width="80%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td><div align="center"> 
                <input name="searchbtn" id="searchbtn" type="submit" class="button">
                <input name="button" type="button" class="button" onClick="closeView('<%=IAMEncoder.encodeJavaScript(uniqueId)%>');" value="Cancel">
              </div></td>
          </tr>
        </table>
      </form></td>
  </tr>
</table>
</body>
</html>


