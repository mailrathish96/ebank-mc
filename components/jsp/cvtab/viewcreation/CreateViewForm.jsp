<%-- $Id$ --%>
<%@ include file='/components/jsp/CommonIncludes.jspf'%>
<%@page import="com.adventnet.iam.xss.IAMEncoder"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Custom View Creation</title>
<%
String path = request.getContextPath();
String comppath = path + "/components/javascript/";
String vcpath = comppath + "viewcreation/";
%>
<%@ page import = "com.adventnet.ds.query.*,com.adventnet.persistence.*, com.adventnet.clientcomponents.*,java.util.*,com.adventnet.client.util.*"%>
<script src='<%=vcpath%>CreateViewForm.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=vcpath%>AutoCompleteQuerySpecific.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=vcpath%>AutoCompleteDataSpecific.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=vcpath%>TmpTableConstants.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=comppath%>DragAndDrop.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=comppath%>Personalization.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<link rel='StyleSheet' HREF='<%= themeDir%>/styles/CustomViewCreation.css' type='text/css' /><%--NO OUTPUTENCODING --%>
<%
String type = request.getParameter("LayoutType");
if(type == null){
	type = "Table";
}
if(type.equals("SingleColumn")){
%>
<script src='<%=comppath%>PropertySheetLayout.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<%
}
else if(type.equals("CPS") || "CPSForm".equals(type)){
	%>
<script src='<%=comppath%>CustomPS.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<%
}
%>
<style>
.MouseHoverStyle {
        border: 1px solid #000000;
}

.MouseOutStyle {
        border: 1px solid #FFFFFF;
}

.TableSelection {
        border: 2px solid BLUE;
        background-color : #ddddff;
}
.wkContent1, .wkContentEdit {
	background-color: #51A9F1;
	padding:2px;
	font: normal 11px Verdana, Arial, Helvetica, sans-serif;
	color: #000000;
	cursor: move;
	text-align:left;
}

.wkContent1 {
	width:150px;
	white-space:nowrap;
	border-top:1px solid #FFFFFF;
	border-left:1px solid #FFFFFF;
	border-bottom:1px solid #000000;
}

.wkContentEdit {
	text-align:right;
	white-space:nowrap;
	border-top:1px solid #FFFFFF;
	border-bottom:1px solid #000000;
	border-right:1px solid #000000;
}

.PSFieldName {
	background-color: #91A981;
}

.PSFieldValue {
	background-color: #91E9F1;
}

.PSText {
	background-color: #CCCCCC;
}

</style>        
<script>
   function setAdvanced()
   {
        var adv =  document.getElementById("advButton");
        adv.value = "Advanced"; 
        
  } 
</script>
<script>
//initialize table 

function showRequiredLinks(linkNames){
	for(var i=0; i< linkNames.length;i++){
		document.getElementById(linkNames[i]).className="";
	}
}

function updateDataObject(type){
	if(type == "CPS" || type == "CPSForm"){
		updateDOForCPS();
	}
}

function initColumns(type)
{
	if(type == "Table"){
		showRequiredLinks(new Array("addRemoveTableColumn"));
		var detRows = daob.getRowsForTable("TmpViewDetails");
		if(detRows && detRows.length == 1)
		{
			constructTableFromDO();
		}
		else
		{
			addColumnToTable("false"); 
		}
	}
	else if(type == "SingleColumn"){
		initPSLayout(daob, 'previewTableContainerRow','SingleColumn');
	}
	else if(type == "CPS" || type == "CPSForm"){
		showRequiredLinks(new Array("undoRedo","addRemoveLayoutColumn"));
		initCPSLayout(daob, 'previewTableContainerRow',type);
		var currentViewNames = new Array();
		var rIndex = 0;
		var cIndex = 0;
		var psTableLayoutCustomization = new Object();
		initMethodContainer("psTableLayoutCustomization");

	}


	//Focus the first column   

	if(firstInputFieldID)
	{ 
		onfocusElem = document.getElementById(firstInputFieldID);
		onfocusElem.className = "TableSelection";
		resetAllUIComponents();
		showColumnPropertiesInUI(onfocusElem.id);
	}

	//initialize the drop down components
	if('<%= IAMEncoder.encodeJavaScript((String)request.getParameter("dsname")) %>' != 'null'  )     
	{
		<% if( request.getParameter("VIEW_DO")  != null)
		{                
			%>     
				initializeComponents("TABLENAME_COLNAME",'handleMoreElements','columnsFromSelectQuery','<%= IAMEncoder.encodeJavaScript((String)request.getParameter("dsname"))   %>') ;            
			<%
		}else
		{
			%>
				initializeComponents("TABLENAME_COLNAME",'handleMoreElements','null','<%= IAMEncoder.encodeJavaScript((String)request.getParameter("dsname"))   %>') ;
			<%
		}
		%>
	}
	else
	{
		<% if( request.getParameter("VIEW_DO")  != null)
		{
			%>          initializeComponents("TABLENAME_COLNAME",'handleMoreElements','columnsFromSelectQuery') ;
			<%
		}else
		{
			%>  
				initializeComponents("TABLENAME_COLNAME",'handleMoreElements','null') ;

			<%
		}

		%>
	}

	initializeAutoFill("LINKVIEW_OR_ACTIONNAME","ISLINKVIEW", "<%= IAMEncoder.encodeJavaScript(path)%>/LinkViewFetchServlet.dat?",'handleMoreElements',15);
}

function columnsFromSelectQuery(sValue,inputFldId)
{
   var selDO = new DataObject();
   
   var rows = daob.getRowsForTable("SelectColumn");
   for(var index in rows)
       {     
           var tableAlias = rows[index]["TABLEALIAS"];   
           var columnName = rows[index]["COLUMNNAME"];   
           var tableAllRows = daob.getRows("SelectColumn","TABLEALIAS",tableAlias); 
           
           
           if(tableAlias.toLowerCase().indexOf(sValue.toLowerCase()) == 0)
           {  
                //addAllColumnInTable(columns,sortedTableColumns);
                if(!selDO.containsTable(tableAlias))
                {
                  addRowsFromObject(selDO,tableAlias,tableAllRows) 
                  
                 }
                
            }
            else
            {
                for(var j=0;j<tableAllRows.length;j++) 
                {
                  if(tableAllRows[j]["COLUMNNAME"].toLowerCase().indexOf(sValue.toLowerCase()) == 0)
                  {
                    if(!selDO.containsTable(tableAlias))
                    {
                        addRowsFromObject(selDO,tableAlias,tableAllRows) 
                     }
                    break;
                  }
                }
            }
       }

      
      txtFldMap[inputFldId].displayFromDO(selDO);
      txtFldMap[inputFldId].showDIVAfterCheck(inputFldId); 
     
     
       

}

 function  addRowsFromObject(selDO,tableAlias,tableAllRows) 
{
    // iterate all the properties in an object to identify the column    
    for(var i in tableAllRows)
   {      
      if(selDO.getFirstRow(tableAlias,"COLUMNNAME",tableAllRows[i]["COLUMNNAME"]) == null)
      {
            var  wholeArray = new Array();        
            wholeArray.push(new Array("COLUMNNAME",tableAllRows[i]["COLUMNNAME"]));
            selDO.addRowsForTable(tableAlias,wholeArray);
       }
   }
}

</script>


</head>
<body onload='initColumns("<%=IAMEncoder.encodeJavaScript(type)%>")' > 

<form  action="<%= IAMEncoder.encodeHTMLAttribute(uniqueId) %>.ve" name="mainForm" method="post"  >
	<input type="hidden" value="" name="xmlData" >
	<input type="hidden" value="<%= request.getAttribute("operationType")   %>" name="operationType" ><%--NO OUTPUTENCODING --%>
	<% 
	if(request.getAttribute("dsname")  != null) {
	%>
		<input type="hidden" value="<%= IAMEncoder.encodeHTMLAttribute((String)request.getAttribute("dsname"))   %>" name="dsname" >
		<%
  }
	%>
	<input type="hidden" value="false" name="viewxml" >
	<input type="hidden" name="viewtitle" >
	<table width="95%"  border="0" align="center" cellpadding="2" cellspacing="4" class="tabBorder">
  	<tr align="center">
    	<td width="15%" nowrap class="tabOn"><img src="<%= themeDir%>/images/step1_on.gif" align="absmiddle">&nbsp;Layout Properties </td><%--NO OUTPUTENCODING --%>
	    <td width="15%" nowrap class="tabOff"><img src="<%= themeDir%>/images/step2_off.gif" align="absmiddle">&nbsp;Criteria</td><%--NO OUTPUTENCODING --%>
  	  <td width="15%" nowrap class="tabOff"><img src="<%= themeDir%>/images/step3_off.gif" align="absmiddle">&nbsp;Page Properties</td><%--NO OUTPUTENCODING --%>
    	<td width="90%">&nbsp;</td>
	  </tr>
	</table>
	<!--table width="90%"  border="0" align="center" cellpadding="2" cellspacing="0">
  	<tr>
	    <td class="notesContent"><strong>Note :</strong> User _$ to fetch dynamic data in each field. </td>
	  </tr>
	</table-->
	<br>
	<table width="90%"  border="0" align="center" cellpadding="2" cellspacing="0">
  	<tr>
    	<td>
				<table width="70%"  border="0" cellpadding="0" cellspacing="0" class="editAreaBorder">
		      <tr>
    		    <td nowrap class="editAreaMenuBar">&nbsp;&nbsp;<B>LAYOUT AREA</B> </td>
		        <td align="right" class="editAreaMenuBar">
							<span id="addRemoveTableColumn" class="hide">
								<a  href="#" onClick="addColumnToTable()">Add Column</a> | 
								<a  href="#" onClick="addColumnToTable('true')">Add Dummy Column</a> |
								<a href="#" onClick="deleteColumnFromTable()"  >Delete Column</a>&nbsp;&nbsp;&nbsp;
							</span>	
					    <span id="undoRedo" class="hide">
								<span id='undoDisable'><input type='button' class='undoDisabled'/>&nbsp;Undo</span>
								<a href="javascript:undo()" id='undoEnable'><input type='button' class='undo'>&nbsp;Undo</a> | 
								<span id='redoDisable'><input type='button' class='redoDisabled'>&nbsp;Redo</span>
								<a id='redoEnable' href="javascript:redo()"><input type='button' class='redo'>&nbsp;Redo</a> | 
							</span>
							<span id="addRemoveLayoutColumn">
								<a href="javascript:addLabel('Label')">Add Label</a> |
								<a href="javascript:addLabel('FieldName')">Add FieldName</a> |
								<a href="javascript:addLabel('Text')">Add Text</a> |
								<a href="javascript:addColumn()">Add Column</a> | 
								&nbsp;<a href="javascript:deleteColumn()">Delete Column</a>&nbsp;&nbsp;&nbsp;
							</span>	
						</td>
    		  </tr>
		      <tr align="center">
		        <td height="90" colspan="2" class="editAreaBg" style="padding:23px;">
    		   	<!---   Preview Table --> 
						<%
							if(type.equals("Table")){
							%>
       					<div style="width: 750px; height:140px; overflow:auto" >
							<%
							}
							else {
							%>
			       		<div style="width: 100%;" >
							<%
							}
							%>
									<table   style="border:1px #FF0000" cellpadding="0" cellspacing="1" bgcolor="#CBC0AB"  >
		            		<tr id="previewTableContainerRow" class="tableHeader"></tr>
	    		    		</table>
        				</div>
  				      <!---   Preview Table --> 
			        </td>
      		</tr>
		    </table>
    
			<table width="90%"  align="center" border="0" cellpadding="2" cellspacing="0" class="confTable" id="COLUMN_PROPERTIES" >
        <tr align="center">
          <td height="24" align="left" class="confHdrBg"><span class="confHdrText">Column Properties</span></td>
        </tr>
        <tr align="center">
          <td height="24" align="left" class="confBg"><table width="100%"  border=0 cellpadding="0" cellspacing="0" class="level1">
              <tr align="right" >
                <td width="34%" align="left" valign="top" class="tableMarginAndBorder"><table width="100%"  border="0" cellpadding="2" cellspacing="0" class="tableMargin">
                    <tr>
                      <td class="confContent"><label for="label">Display Name:</label>
                          <br>
                          <input name="DISPLAYNAME" type="text" class="txtField" id="label" size="30" onkeyup="keyUpUpdate(this,'DISPLAYNAME')" ></td>
                    </tr>
                  </table>                  
                    <table width="100%"  border="0" cellpadding="2" cellspacing="0" class="tableMargin">
                      <tr>
                        <td width="38%" nowrap class="confContent">Link / Action :<br>
                            <input name="LINKVIEW_OR_ACTIONNAME" type="text" class="txtField" size="35">
                            <input name="ISLINKVIEW" id="ISLINKVIEW" type="hidden" class="txtField"   size="25"></td>
                        <td width="62%" nowrap class="confContent">&nbsp;</td>
                      </tr>
                  </table></td>
                <td width="37%" align="left" valign="top" nowrap class="tableMarginAndBorder"><table width="100%"   cellpadding="2" cellspacing="0" class="tableMargin">
                    <tr>
                      <td nowrap class="confContent">
                          <label  id="DBColumn">DB Column :</label>
                          <br>
                          <input name="TABLENAME_COLNAME" id="TABLENAME_COLNAME" type="text" class="txtField" size="30"></td>
                    </tr>
                    <tr>
                      <td nowrap class="confContent">Open In<br>
                          <select name="select3" class="txtField" disabled>
                            <option selected>Same Window</option>
                            <option>New Window</option>
                            <option>In-line View [Swipe]</option>
                            <option>Pop Up Window</option>
                        </select></td>
                    </tr>
                </table></td>
                <td width="29%" valign="top" class="confContent"><table width="100%"   cellpadding="0" cellspacing="0">
                    <tr class="confContent">
                      <td><input type="checkbox" name="VISIBLE" value="true" onclick="focusLost(); updatePreview()"></td>
                      <td width="99%" nowrap>Show by default </td>
                    </tr>
                    <tr>
                      <td><input type="checkbox" name="SORTENABLED" value="true" onclick="focusLost(); updatePreview()" ></td>
                      <td nowrap> Sorting</td>
                    </tr>
                    <tr>
                      <td><input type="checkbox" name="SEARCHENABLED" value="false" onclick="focusLost(); updatePreview()" ></td>
                      <td nowrap>
                        <label for="label40">Searching</label></td>
                    </tr>
                    
                    
                  </table>
                    <table width="100%"  border="0" cellspacing="0" cellpadding="2">
                      <tr class="confContent">
                        <td>&nbsp; </td>
                      </tr>
                  </table></td>
              </tr>
							<%
							if(type.equals("CPSForm")){
								SelectQuery sq = new SelectQueryImpl(new Table(ACELEMENT.TABLE));
								sq.addSelectColumn(new Column(null, "*"));
								DataObject dataObject = LookUpUtil.getPersistence().get(sq);
								Iterator rows = dataObject.getRows(ACELEMENT.TABLE);
								%>
								<tr>
									<td class="confContent">
										Creator Config 
										<select name="creatorConfig" onChange="updateCreatorConfig(this)">
											<option name="None">-------------------------</option>
											<%
											while(rows.hasNext()){
												Row currentRow = (Row) rows.next();
												%>
													<option name='<%=IAMEncoder.encodeHTMLAttribute((String)currentRow.get("NAME"))%>'><%=IAMEncoder.encodeHTML((String)currentRow.get("NAME"))%></option>
												<%
											}
											%>
										</select>
									</td>
								</tr>	
								<%
							}
							%>
              <tr align=right>
                      <td colspan='3' width="60%" align="right" nowrap  ><input type="button" name="advButton"   value="Advanced" onclick='toggleAdvanced()' >                      
                      </td>
             </tr>

            </table>
            <Div id="advanced" style="display: none;" >
              <table width="100%"  border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="33%" valign="top" class="tableMarginAndBorder"><table width="100%"  border="0" cellspacing="0" cellpadding="2">
                      <tr>
                        <td nowrap class="confContent">Prefix Text: </td>
                        <td width="99%"><input name="PREFIX_TEXT" type="text" class="txtField"  size="30"  onkeyup="keyUpUpdate(this,'PREFIX_TEXT')"   ></td>
                        <td width="99%">&nbsp;</td>
                      </tr>
                      <tr>
                        <td nowrap class="confContent">Suffix Text: </td>
                        <td><input name="SUFFIX_TEXT" type="text" class="txtField" size="30" onkeyup="keyUpUpdate(this,'SUFFIX_TEXT')"  ></td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="confContent">Format:</td>
                        <td><select name="DATE_FORMAT" id="DATE_FORMAT" class="txtField">
                            <option value="" selected>None</option> 
                            <option value="EEE, MMM dd yyyy" >Thursday, April 05 2005</option>
                            <option value="MMM dd, yyyy">April 05, 2005</option>
                            <option value="dd-MMM-yyyy">05-Apr-2005</option>
                        </select></td>
                        <td>&nbsp;</td>
                      </tr>
                  </table></td>
                  <td width="99%" valign="top" class="confContent"><table width="100%"  border="0" cellpadding="2" cellspacing="0">
                      <tr>
                        <td nowrap class="confContent">Prefix Icon:</td>
                        <td width="99%"><input name="ICON" type="text" class="txtField" onkeyup="keyUpUpdate(this,'PREFIX_ICON')" size="30"></td>
                      </tr>
                      <tr>
                        <td nowrap class="confContent"><label for="label37">Suffix Icon:</label></td>
                        <td><input name="SUFFIX_ICON" type="text" class="txtField"  onkeyup="keyUpUpdate(this,'SUFFIX_ICON')"  size="30"></td>
                      </tr>
                      <tr>
                        <td nowrap class="confContent">Default Text: </td>
                        <td><input id="DEFAULT_TEXT" name="DEFAULT_TEXT" type="text" class="txtField" size="30"></td>
                      </tr>
                  </table></td>
                </tr>
            </table>
           </Div> 
          </td>
        </tr>
      </table></td>
  </tr>
</table>
<br>
<br>
<table width="90%"  border="0" align="center" cellpadding="4" cellspacing="0" class="btnPanel">
  <tr>
    <td width="55%" align="left" nowrap><!--input type="button" name="Submit3" value="&lt;&lt; Back"-->
        <!--input type="button" name="Submit4" value="Next &gt;&gt;"-->
        
    </td>
    <td width="60%" align="left" nowrap>&nbsp;
        <input name="Submit" type="button" value="Save" onclick="this.form.submitted = true; document.mainForm.viewxml.value = 'false';updateDataObject('<%=IAMEncoder.encodeJavaScript(type)%>'); handleSubmit(); return false;">
        <input name="Submit2" type="button" value="Cancel">        
    </td>
  </tr>
  
  
</table>
</form>


</body>
</html>

<div id="movableDiv" class='hide' style="width:175px;background-color:transparent; position: absolute; cursor: default;filter:alpha(opacity=75);-moz-opacity:0.5;opacity: 0.9;">
	<table cellspacing='0' cellpadding='0' width='175px'>
		<tr>
			<td class="smContent" id='movableDivContent'></td>
		</tr>
	</table>
</div>	

<DIV id='GridView' class='hide'>
	<TABLE border='0' cellpadding='0' cellspacing='0' width='100%' onMouseOver='enableViewSelection(document.getElementById("_ev$ID"))'>
		<TR>
			<td colspan="3">
				<div id="_indicator1$ID" style="float:left;position:relative;top:-2;width:100%;" class='hide'>&nbsp;</div>
				
        <div viewId = '$ID' viewName='$VN' title='$TITLE' desc='$DESC' id="_indicator2$ID" style="float:left;position:relative;top:-2;width:100%;border-left:2px solid #000000;height:12px; border-right:2px solid #000000;background-image:url(<%=themeDir%>/images/insertIndication.gif);background-repeat:repeat-x " class='hide'>&nbsp;</div><%--NO OUTPUTENCODING --%> 
				
        <div id="_indicator3$ID" style="float:left;position:relative;top:-2;width:100%;" class='hide'>&nbsp;</div></td>
		</TR>	
		<TR>
			<td colspan='2' class="wkContent1" id='_ev$ID' nowrap viewId = '$ID' onMouseDown="startDrag(event, '_ev$ID')" viewName='$VN' title='$TITLE' desc='$DESC'>&nbsp;<input name="Button" type="button" class="wkDragButton" value=" " style='cursor:move'>&nbsp;&nbsp;&nbsp;<span id="dispName$TITLE">$DISPLAYNAME</span></td>
     	<td width='20px'class="wkContentEdit"><input id="$TITLE" value="E" name="Submit" type="button" class="closeButton" onClick='showColumnPropertiesInUI("$TITLE",false); onfocusElem=this;'></td>
		</tr>
	</table>	
</DIV>

<div id="Testing"></div>

<DIV id='EditMode' class='hide'>
	<TABLE width='100%' border='0' align="center" cellpadding='0' cellspacing='0' class='resizeView'>
		<TR>
			<TD>&nbsp;</td>
			<TD align="center" nowrap>$TOPIMAGES</TD>
			<TD>
				<button onClick='javascript:disableEdit()' class='backButton'>B</button>
			</TD>
		</TR>	
		<TR>
			<TD width='1px'>$LEFTIMAGES</TD>
			<TD align='center' height='$HEIGHTpx'>$IPTITLE</TD>
			<TD width='1px' align="right">$RIGHTIMAGES</TD>
		</TR>
		<TR>
			<TD>&nbsp;</td>
			<TD align="center" nowrap>$BOTTOMIMAGES</TD>
			<TD>&nbsp;</TD>
		</TR>	
	</TABLE>
</DIV>

<DIV id='PlacedView' class='hide'>
	<TABLE border='0' cellpadding='0' cellspacing='0' width='100%' onMouseOver='enableViewSelection(document.getElementById("_ev$ID"))'>
		<TR>
			<td colspan="3">
				<div id="_indicator1$ID" style="float:left;position:relative;top:-2;width:100%;" class='hide'>&nbsp;</div>
				
        <div viewId = '$ID' viewName='$VN' title='$TITLE' desc='$DESC' id="_indicator2$ID" style="float:left;position:relative;top:-2;width:100%;border-left:2px solid #000000;height:12px; border-right:2px solid #000000;background-image:url(<%=themeDir%>/images/insertIndication.gif);background-repeat:repeat-x " class='hide'>&nbsp;</div><%--NO OUTPUTENCODING --%>
				
        <div id="_indicator3$ID" style="float:left;position:relative;top:-2;width:100%;" class='hide'>&nbsp;</div></td>
		</TR>	
		<TR>
			<td colspan="2" class="wkContent1 $CLASS" id='_ev$ID' nowrap viewId = '$ID' onMouseDown="startDrag(event, '_ev$ID')" viewName='$VN' title='$TITLE' desc='$DESC'>&nbsp;<input name="Button" type="button" class="wkDragButton" value=" " style='cursor:move'><input name="Submit" type="button" class="resizeButton" value="R" onClick='return enableEdit($ID)'>&nbsp;&nbsp;&nbsp;$TITLE</td>
     	<td width='20px'class="wkContentEdit $CLASS"><input id="$TITLE" value="E" name="Submit" type="button" class="closeButton" onClick='$METHOD'><input type="button" class="$CSS" onClick='removeView($ID, true)' value="X"></td>
		</tr>
	</table>	
</DIV>

<DIV id='DummyView' class='hide'>
	<TABLE border='0' cellpadding='3' class='emptyCell' cellspacing='0' width='100%' onMouseOver='invokeMethod("enableViewSelection",document.getElementById("_ev$ID_dv"))'>
		<TR><TD  height='$HEIGHTpx' id='_ev$ID_dv' rowIndex='$RID' colIndex='$CID' viewId = '$ID' align="center" viewName='$VN' title='$TITLE' desc='$DESC'>(Empty Cell)</TD></TR>
	</TABLE>	
</DIV>

<DIV id="RemovedGridView" class="hide">
</DIV>
