<%-- $Id$ --%>
<%@ include file='/components/jsp/CommonIncludes.jspf'%>
<%@ page import="com.adventnet.iam.xss.IAMEncoder"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Custom View Creation</title>
<%
String path = request.getContextPath();
String comppath = path + "/components/javascript/";
String vcpath = comppath + "viewcreation/";
String layoutType = request.getParameter("LayoutType");
if(layoutType == null){
	layoutType = "CPSForm";
}
%>
<%@ page import = "com.adventnet.ds.query.*,com.adventnet.persistence.*, com.adventnet.clientcomponents.*,java.util.*,com.adventnet.client.util.*"%>
<script src='<%=vcpath%>CreateViewForm.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=vcpath%>AutoCompleteQuerySpecific.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=vcpath%>AutoCompleteDataSpecific.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=vcpath%>TmpTableConstants.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=comppath%>DragAndDrop.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=comppath%>Personalization.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<link rel='StyleSheet' HREF='<%= themeDir%>/styles/CustomViewCreation.css' type='text/css' /><%--NO OUTPUTENCODING --%>
<script src='<%=comppath%>CustomPS.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<style>
.wkContent1, .wkContentEdit {
	background-color: #51A9F1;
	padding:2px;
	font: normal 11px Verdana, Arial, Helvetica, sans-serif;
	color: #000000;
	cursor: move;
	text-align:left;
}

.wkContent1 {
	width:150px;
	white-space:nowrap;
	border-top:1px solid #FFFFFF;
	border-left:1px solid #FFFFFF;
	border-bottom:1px solid #000000;
}

.wkContentEdit {
	text-align:right;
	white-space:nowrap;
	border-top:1px solid #FFFFFF;
	border-bottom:1px solid #000000;
	border-right:1px solid #000000;
}

.PSFieldName {
	background-color: #91A981;
}

.PSFieldValue {
	background-color: #99AACC;
}

.PSText {
	background-color: #CCCCCC;
}

.PSButtonPanel {
	background-color: #999999;
}

</style>        
</head>
<body> 

	<% 
	if(request.getAttribute("dsname")  != null) {
	%>
		<input type="hidden" value="<%= request.getAttribute("dsname")   %>" name="dsname" ><%--NO OUTPUTENCODING --%>
		<%
  }
	%>
	<table width="95%"  border="0" align="center" cellpadding="2" cellspacing="4" class="tabBorder">
  	<tr align="center">
    	<td width="15%" nowrap class="tabOn"><img src="<%= themeDir%>/images/step1_on.gif" align="absmiddle">&nbsp;Layout Properties </td><%--NO OUTPUTENCODING --%>
	    <td width="15%" nowrap class="tabOff"><img src="<%= themeDir%>/images/step2_off.gif" align="absmiddle">&nbsp;Criteria</td><%--NO OUTPUTENCODING --%>
  	  <td width="15%" nowrap class="tabOff"><img src="<%= themeDir%>/images/step3_off.gif" align="absmiddle">&nbsp;Page Properties</td><%--NO OUTPUTENCODING --%>
    	<td width="90%">&nbsp;</td>
	  </tr>
	</table>
			<%@ include file='viewTemplates.html'%>
	<table width="95%"  border="0" cellpadding="0" cellspacing="0" align="center" class="CustomCPS">
  	<tr>
    	<td>
				<br>
				<table width="70%"  border="0" cellpadding="0" cellspacing="0" class="editAreaBorder">
		      <tr>
    		    <td nowrap class="editAreaMenuBar">&nbsp;&nbsp;<B>LAYOUT AREA</B> </td>
		        <td align="right" class="editAreaMenuBar">
								<span id='undoDisable'><input type='button' class='undoDisabled'/>&nbsp;Undo</span>
								<a href="javascript:undo()" id='undoEnable'><input type='button' class='undo'>&nbsp;Undo</a> | 
								<span id='redoDisable'><input type='button' class='redoDisabled'>&nbsp;Redo</span>
								<a id='redoEnable' href="javascript:redo()"><input type='button' class='redo'>&nbsp;Redo</a> | 
								<a href="javascript:addColumn()">Add Column</a> | 
								&nbsp;<a href="javascript:deleteColumn()">Delete Column</a>&nbsp;&nbsp;&nbsp;
						</td>
    		  </tr>
		      <tr align="center">
		        <td height="90" colspan="2" class="editAreaBg" style="padding:23px;">
    		   	<!---   Preview Table --> 
			       		<div style="width: 100%;" >
									<table   style="border:1px #FF0000" cellpadding="0" cellspacing="1" bgcolor="#CBC0AB"  >
		            		<tr id="previewTableContainerRow" class="tableHeader"></tr>
										<script>
											initCPSLayout(daob, 'previewTableContainerRow','<%=IAMEncoder.encodeJavaScript(layoutType)%>');
										</script>
	    		    		</table>
        				</div>
  				      <!---   Preview Table --> 
			        </td>
      		</tr>
		    </table>
			</td>
			<td width='10px' id='_HIDEPANE' class="paneBg show" rowspan="2"><input type='button' class='hideSideBar' onClick='hidePane()'></td>
			<td width='10px' id='_SHOWPANE' class="paneBg hide" rowspan="2"><input type='button' class='showSideBar' onclick='showPane()'></td>
			<td id = '_PANE' width="24%" valign="top" class="rightBg" rowspan="2">
			<%@ include file='RightBar.jspf'%>
			</td>
		</tr>
		<tr>
			<td>
			<br>
				<form  action="<%= IAMEncoder.encodeHTMLAttribute(uniqueId) %>.ve" name="mainForm" method="post"  ><%--NO OUTPUTENCODING --%>
					<input type="hidden" value="" name="xmlData" >
					<input type="hidden" value="<%= request.getAttribute("operationType")   %>" name="operationType" ><%--NO OUTPUTENCODING --%>
					<input type="hidden" value="false" name="viewxml" >
					<input type="hidden" name="viewtitle" value='<%=IAMEncoder.encodeHTMLAttribute(request.getParameter("VIEWNAME"))%>'>
					<table width="90%"  align="center" border="0" cellpadding="2" cellspacing="0" class="confTable" id="COLUMN_PROPERTIES" >
		        <tr align="center">
		          <td height="24" align="left" class="confHdrBg" colspan="4"><span class="confHdrText">Column Properties </span><span id="selCol"></span></td>
		        </tr>
						<tr>
  	          <td valign="top" class="tableMarginAndBorder" colspan="4">
						</tr>
						<tr id="LinkActionField" class="hide">
							<td nowrap class="confContent">Link / Action</td>
							<td>
								<input name="LINKVIEW_OR_ACTIONNAME" type="text" class="txtField" size="35">
								<input name="ISLINKVIEW" id="ISLINKVIEW" type="hidden" class="txtField"   size="25">
							</td>
							<td nowrap class="confContent">Open In</td>
							<td>
								<select name="select3" class="txtField" disabled>
                	<option selected>Same Window</option>
              	  <option>New Window</option>
             	   	<option>In-line View [Swipe]</option>
                	<option>Pop Up Window</option>
                </select>
							</td>
						</tr>
						<tr id="SuffixPrefixFields" class="hide">
							<td nowrap width="15%" class="confContent">Prefix Text: </td>
        	    <td><input name="PREFIX_TEXT" type="text" class="txtField"  size="30"  onkeyup="updatePropsForFormColumn(this)"></td>
              <td nowrap width="15%" class="confContent">Prefix Icon:</td>
              <td><input name="ICON" type="text" class="txtField" onkeyup="updatePropsForFormColumn(this)" size="30"></td>
						</tr>
						<tr id="SuffixPrefixIcons" class="hide">
							<td nowrap class="confContent">Suffix Text: </td>
              <td><input name="SUFFIX_TEXT" type="text" class="txtField" size="30" onkeyup="updatePropsForFormColumn(this)"></td>
              <td nowrap class="confContent"><label for="label37">Suffix Icon:</label></td>
              <td><input name="SUFFIX_ICON" type="text" class="txtField"  onkeyup="updatePropsForFormColumn(this)"  size="30"></td>
            </tr>
						<tr id="DateFormatField" class="hide">
							<td class="confContent">Format:</td>
              <td>
								<select name="DATE_FORMAT" id="DATE_FORMAT" class="txtField" onChange="updatePropsForFormColumn(this)">
									<option value="" selected>None</option> 
                  <option value="EEE, MMM dd yyyy" >Thursday, April 05 2005</option>
                  <option value="MMM dd, yyyy">April 05, 2005</option>
                  <option value="dd-MMM-yyyy">05-Apr-2005</option>
                </select>
							</td>
              <td nowrap class="confContent">Default Text: </td>
						  <td><input id="DEFAULT_TEXT" name="DEFAULT_TEXT" type="text" class="txtField" size="30"></td>
            </tr>
						<tr id="DisplayNameField" class="hide">
							<td nowrap width="15%" class="confContent">Display Name </td>
        	    <td><input name="TITLE" type="text" class="txtField"  size="30"  onkeyup="updateDisplayNameforFormElement(this)"></td>
							<td nowrap width="15%" class="confContent">Label Type </td>
							<td>
								<select name="labelType" onChange="updateLabelType(this)">
									<option value="Label">Label</option>
									<option value="FieldName">Field Name</option>
									<option value="Text">Text</option>
									<option value="ButtonPanel">Button Panel</option>
								</select>
							</td>
						</tr>	
						<tr id="CreatorConfig" class="hide">
    					<%
							SelectQuery sq = new SelectQueryImpl(new Table(ACELEMENT.TABLE));
							sq.addSelectColumn(new Column(null, "*"));
							DataObject dataObject = LookUpUtil.getPersistence().get(sq);
							Iterator rows = dataObject.getRows(ACELEMENT.TABLE);
							%>
							<td class="confContent"> Creator Config</td>
							<td>
								<select name="creatorConfig" onChange="updateCreatorConfig(this)">
									<option name="None">-------------------------</option>
									<%
										while(rows.hasNext()){
											Row currentRow = (Row) rows.next();
											%>
											<option name='<%=IAMEncoder.encodeHTMLAttribute((String)currentRow.get("NAME"))%>'><%=IAMEncoder.encodeHTML((String)currentRow.get("NAME"))%></option>
											<%
										}
									%>
								</select>
							</td>
						</tr>

					</table>
					<br><br>
					<table width="90%"  border="0" align="center" cellpadding="4" cellspacing="0" class="btnPanel">
					  <tr>
					    <td align="center" nowrap>&nbsp;
			        <% 
							if(! (request.getParameter("viewxml") != null &&  request.getParameter("viewxml").equalsIgnoreCase("false"))) 
			        { 
								%>
				        <input type="button" name="view_xml" value="View Xml" onclick='this.form.submitted = true; document.mainForm.viewxml.value = "true" ;handleSubmit(); return false;' >  
        				<%
			        }
							%>
							<input name="Submit" type="button" value="Save" onclick="this.form.submitted = true; document.mainForm.viewxml.value = 'false';updateDOForCPS(); handleSubmit(); return false;">
		        <input name="Submit2" type="button" value="Cancel">        
			    	</td>
				  </tr>
				</table>
			</form>
		</td>
	</tr>
</table>
<%
if(layoutType.equals("CPS")){
%>
<script>
	initializeAutoFill("LINKVIEW_OR_ACTIONNAME","ISLINKVIEW", "<%= path%>/LinkViewFetchServlet.dat?",'handleMoreElements',15);<%--NO OUTPUTENCODING --%>
</script>	
<%
}
%>

</body>
</html>


