<!-- $Id$ -->
<%@page import="com.adventnet.client.themes.web.ThemesAPI"%><%@page import="com.adventnet.client.util.web.WebClientUtil"%><%@page import="com.adventnet.client.view.web.StateAPI"%><%@page import="com.adventnet.authentication.util.*"%><%@page import="com.adventnet.persistence.*"%><%@page import="com.adventnet.client.components.mailer.Mailer"%><% String themeDir = ThemesAPI.getThemeDirForRequest(request); long userId = AuthUtil.getUserCredential().getUserId(); String feedBackType = (String)request.getAttribute("FeedBackType"); if(feedBackType == null) { feedBackType = request.getParameter("FeedBackType"); } if(feedBackType == null) { feedBackType = "feedBack"; } String thanksTitle = " Instant FeedBack"; if("Error".equals(feedBackType)) { thanksTitle = " Error Report"; }%>
<HTML>
<HEAD>
<TITLE><%= thanksTitle %></TITLE>
<META content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<link href="<%=themeDir%>/styles/style.css" rel="stylesheet" type="text/css"><script src='<%=request.getContextPath()%>/components/javascript/FormHandling.js' type='text/javascript'></script>
</HEAD>
<% if(request.getParameter("Send") != null){ if(!request.getParameter("FROMADDRESS").equals(request.getParameter("DEFAULTFROMADDRESS"))){ session.setAttribute("FROMADDRESS", request.getParameter("FROMADDRESS")); } Mailer.submitFeedback(request);%><%@ include file="instantFeedbackThanks.jspf" %><% } else { String fromAddress = (String) session.getAttribute("FROMADDRESS"); if(fromAddress == null){ fromAddress = Mailer.getFromAddress(request, userId); } if(fromAddress == null){ fromAddress = ""; }%>
<table width="100%" cellpadding="0" cellspacing="0" class="productLogo" height="30"><% String title = "FeedBack Form"; if("Error".equals(feedBackType)) { title = "Error Report"; }%><tr><td class="titleText"><%= title %></td></tr></table> <form name="feedBackFrm" action="<%=request.getContextPath()%>/Feedback.do" method="post"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="oddRow"><tr><td> <table border="0" cellpadding="0" cellspacing="0" width="100%" height="90" class="oddRow"><tr><td height="20" width="8%">&nbsp;</td> <td width="25%" height="22"><strong>&nbsp;&nbsp;From&nbsp;:</strong></td> <td width="72%" height="30"> <input class="txtbox" name="FROMADDRESS" maxlength="100" size="30" value = "<%=fromAddress%>" validatemethod = "isEmailId" errormsg="Please enter valid message for From Address">
<input type="hidden" name="DEFAULTFROMADDRESS" value = "<%=fromAddress%>"> </td></tr>
<tr><td height="20" width="8%">&nbsp;</td> <td width="25%" height="22"><strong>&nbsp;&nbsp;Subject&nbsp;:</strong></td> <td width="72%" height="30"> <input class="txtbox" maxlength="100" name="SUBJECT" size="45" validatemethod="isNotEmpty" errormsg="Please enter subject"> </td></tr>
<tr><td height="25" width="8%">&nbsp;</td> <td height="22" width="25%" valign="top" nowrap> <strong>&nbsp;&nbsp;Comments :</strong></td>
<td width="72%"  height="22" valign="top"> <textarea name="COMMENTS" class="txtbox" wrap="soft" rows="7" cols="40" validatemethod="isNotEmpty" errormsg="Please enter comments."></textarea> 
<input type="hidden" name="TOADDRESS" value="<%=Mailer.getToAddress(request)%>">
<input type="hidden" name="USERNAME" value="<%=AuthUtil.getUserCredential().getLoginName()%>">
<input type="hidden" name="JavaVersion" value='<%=System.getProperty("java.version")%>'>
<input type="hidden" name="Operating System" value='<%=System.getProperty("os.name")%>'>
<input type="hidden" name="BrowserVersion" value="">
<input type="hidden" name="STACKTRACE" value="">
<input type="hidden" name="FeedBackType" value="<%=feedBackType%>">
<input type="hidden" name="CLIENTSTATE" value="<%=StateAPI.getClientRequestStateAsString(request)%>">
<input type="hidden" name="REQUESTEDURL" value="">
<script>document.feedBackFrm.BrowserVersion.value = navigator.userAgent;
<% if("Error".equals(feedBackType)){%>
document.feedBackFrm.REQUESTEDURL.value = window.location.href;
<% } else{%> 
if(window.opener != null)
{ 
document.feedBackFrm.REQUESTEDURL.value = window.opener.location.href;		
}
<% }%></script> </td></tr>
<% if("Error".equals(feedBackType)) {%><tr><td colspan="2"></td><td><b>Note:&nbsp;<font color="red">Error Details has already been attached with the mail</font></b></td></tr> <% }%></table> </td></tr>
<tr><td bgcolor="#666666"><img src="images/trans.gif" width="1" height="1"></td></tr></table> <table width="100%" border="0" cellspacing="0" cellpadding="5"><tr><td class="btnPanel"> <input name="Send" type="submit" style="width:80" class="btnStyle" value="Send"  alt="Send" onClick='return validateForm(this.form)'> 
<input name="Submit" type="button" style="width:80" class="btnStyle" onClick="window.close()" value="Close"> </td></tr></table></form>
<% }%>
</HTML>
