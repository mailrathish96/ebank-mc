<!-- $Id$ -->
<%@ page import="com.adventnet.clientcomponents.*"%>
<%@ page import="com.adventnet.client.action.web.MenuVariablesGenerator"%>
<%@ page import="com.adventnet.client.components.table.template.FillTable"%>
<%@page import="com.adventnet.iam.xss.IAMEncoder"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="mc_table">
<%@ include file='../CommonIncludes.jspf'%>

<%@ include file='InitTableVariables.jspf'%>
<%
String html="";
String templateViewName="";
if((viewModel.getTableViewConfigRow().get("TEMPLATEVIEWNAME"))!=null){ //No I18N
 templateViewName=(String)(viewModel.getTableViewConfigRow().get("TEMPLATEVIEWNAME")); //No I18N
}
 boolean hideHeaderNavig=false;
int rowcount = tableModel.getRowCount();
boolean hide=(((Boolean)(viewModel.getTableViewConfigRow().get("HIDEHEADERANDNAVIG")))).booleanValue(); //No I18N
if((((tableModel instanceof TableNavigatorModel) && rowcount<=0) && hide)) {
hideHeaderNavig=true;
 }
if(((Boolean)(viewModel.getTableViewConfigRow().get("ENABLEEXPORT"))).booleanValue()) { //No I18N
html=com.adventnet.client.tpl.TemplateAPI.givehtml("Table_Export",null,new Object[][] {{"VIEWNAME",viewContext.getUniqueId()}}); //No I18N
}
boolean ajaxTableUpdate=false;
if(request.getParameter("ajaxTableUpdate")!=null) {
if(request.getParameter("ajaxTableUpdate").equals("true")) {
ajaxTableUpdate=true;
}
}
 if(!hideHeaderNavig) {%>
<%= html %><%--NO OUTPUTENCODING --%><%
} if((!ajaxTableUpdate)&&(!(FillTable.tablehtmlmap.containsKey(viewContext.getUniqueId()))) && (!(FillTable.tablehtmlmap.containsKey(templateViewName)))) {
if(!hideHeaderNavig) {%>
<%@ include file='DisplayMenu.jspf'%>
<table width="100%" border="0" cellpadding="0" cellspacing="0" >
    <tr class="navigatorTable" >
       <td nowrap align="left">
            <%
                String menuListurl = (String)viewContext.getModel().getFeatureValue("MENU_INCLUDES"); //NO I18N
                request.setAttribute("MENU_INCLUDES",viewContext.getModel().getFeatureValue("MENU_INCLUDES"));
             %>
              <% if(menuListurl!=null) {%>
             <jsp:include page="<%= menuListurl %>" />
             <% }%>
        </td>
        <% if(WebClientUtil.isRequiredUrl(viewContext)) {%>
        <td style="width:128px">
            <div class="mc_top"><%@ include file="/components/jsp/table/IncludeNavigation.jspf"%></div>
        </td>

        <% }%>
    </tr>
</table>
<%@ include file="/components/jsp/table/IncludeFrmAndTblDec.jspf"%>
<% String displayType = (String) viewModel.getTableViewConfigRow().get("DISPLAYTYPE");
if(displayType.equals("Horizontal")) {%>
<%@ include file = "DisplayTableHorizontally.jspf" %>
<% } else if(displayType.equals("Vertical")) {%>
<%@ include file = "DisplayTableVertically.jspf" %><% } else if(displayType.equals("PropertySheet")) {%>
<%@ include file = "DisplayTableAsRecords.jspf" %><% }%>
<%@ include file="/components/jsp/table/EndFrmAndTblDec.jspf"%>
<% if(!isScrollEnabled && !("true".equals(request.getAttribute("NO_NOROW")))) {%>
<div class="mc_nodata"><%@ include file = "/components/jsp/table/DisplayNoRowsMessage.jspf" %></div><%--No I18N--%> 
 <% }
 if(WebClientUtil.isRequiredUrl(viewContext) && !hideHeaderNavig) {%>
 <div class="mc_navi"><%@ include file="/components/jsp/table/IncludeNavigation.jspf"%></div>
<% }
} else if(!isScrollEnabled) {%>
 <%@ include file = "/components/jsp/table/DisplayNoRowsMessage.jspf" %><% } } else if(!ajaxTableUpdate) {%>
 <%@ include file = "/components/jsp/table/SearchCheck.jspf"%>
<% boolean navigationPresent = true;
 String Html="";
if((!(FillTable.tablehtmlmap.containsKey(templateViewName))) || ((FillTable.tablehtmlmap.containsKey(viewContext.getUniqueId())))) {
templateViewName="";
}
Html=com.adventnet.client.components.table.template.GetModelGiveoutput.produceFilledHtmlOutput(viewModel,viewContext,pageContext,templateViewName,hideHeaderNavig); navigationPresent = "true".equals(viewContext.getStateParameter("NAVIGATIONPRESENT"));
%>
<div class="hide">
   <%@ include file='DisplayMenu.jspf'%>
</div>
<% if(!hideHeaderNavig && !navigationPresent) {%>
<%@ include file="/components/jsp/table/IncludeNavigation.jspf"%>
<% }%><%=Html%><%--NO OUTPUTENCODING --%>
<%@ include file = "/components/jsp/table/DisplayNoRowsMessage.jspf" %>
<% if(!hideHeaderNavig && !navigationPresent) {%>
<%@ include file="/components/jsp/table/IncludeNavigation.jspf"%><% }%>
<% } else {%>
<%@ include file="/components/jsp/table/AjaxUpdate.jspf"%>
<% } if(isScrollEnabled && !ajaxTableUpdate) {
String initialFetchedRows1 = request.getParameter("initialFetchedRows");//No I18N
%>
<script defer>
    handleWidthOfScrollTable("<%=uniqueId%>", "<%=colCount%>");<%--NO OUTPUTENCODING --%>
    addToOnLoadScripts("updateStatus('<%=uniqueId%>','<%=IAMEncoder.encodeJavaScript(initialFetchedRows1)%>')", window);<%--NO OUTPUTENCODING --%>//NO I18N
    <% if(isDynamicTable) {%><%--No I18N --%>
    clearWindowObjects("<%=uniqueId%>");<%--NO OUTPUTENCODING --%>
    handleDynamicTable("<%=uniqueId%>", "<%=colCount%>", '<%=request.getAttribute("initialFetchedRows")%>');<%--NO OUTPUTENCODING --%><% }%></script><% } if(!("true".equals(request.getParameter("ajaxReplace")))) {%>
<div id="<%=uniqueId%>_hidden" class="hide"></div><%--NO OUTPUTENCODING --%><% }%>
</div>