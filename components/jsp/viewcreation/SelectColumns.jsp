<%-- $Id$ --%>
<%@ include file='/components/jsp/CommonIncludes.jspf'%>
<%@ page import ="com.adventnet.client.util.web.WebClientUtil"%>
<%@page import="com.adventnet.iam.xss.IAMEncoder"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Custom View Creation</title>
<%
String path = request.getContextPath();
String comppath = path + "/components/javascript/";
String vcpath = comppath + "viewcreation/";
String layoutType = (String)request.getAttribute("LayoutType");
if(layoutType == null){
	layoutType = request.getParameter("LayoutType");
	if(layoutType == null){
		layoutType = "CPSForm";
	}
}
%>
<%@ page import = "com.adventnet.ds.query.*,com.adventnet.persistence.*, com.adventnet.clientcomponents.*,java.util.*,com.adventnet.client.util.*"%>
<script src='<%=vcpath%>CreateViewForm.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=vcpath%>AutoCompleteQuerySpecific.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=vcpath%>AutoCompleteDataSpecific.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=vcpath%>TmpTableConstants.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=vcpath%>DO.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=comppath%>DragAndDrop.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=comppath%>Personalization.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<link rel='StyleSheet' HREF='<%= themeDir%>/styles/CustomViewCreation.css' type='text/css' /><%--NO OUTPUTENCODING --%>
<script src='<%=comppath%>CustomPS.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<%=request.getAttribute("JSCODE")%><%--NO OUTPUTENCODING --%>
</head>
<%
if(request.getParameter("VIEWNAME") != null){
	%>
		<script>
		setExistingViewName('<%=IAMEncoder.encodeJavaScript(request.getParameter("VIEWNAME"))%>', 'ExistingViewName');
	</script>	
		<%
}
%>
<body> 
<div id="Testing"></id>
<% 
if(request.getAttribute("dsname")  != null) {
	%>
		<input type="hidden" value="<%= request.getAttribute("dsname")   %>" name="dsname" ><%--NO OUTPUTENCODING --%>
		<%
}
%>
<%@ include file='../cvtab/viewcreation/viewTemplates.html'%>
<%@ include file='/framework/jsp/StatusMsg.jspf'%>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="CustomCPS">
	<tr>
		<td valign='top'>
			<table width="100%" cellspacing="0" class="editAreaBorder" valign='top'>
				<tr>
				<%
				if(request.getParameter("VIEWNAME") != null){
					%>
					<td nowrap class="editAreaMenuBar"><strong>Layout Area</strong> - <%=IAMEncoder.encodeHTML(request.getParameter("VIEWNAME"))%></td>
					<%
				}
				else {
					%>
					<td nowrap class="editAreaMenuBar"><strong>Layout Area</strong></td>
					<%
				}
				%>
					<td align="right" class="editAreaMenuBar"><a href="javascript:editTableProperties()">Edit Table Properties</a>&nbsp;&nbsp;&nbsp;
						<span id='undoDisable'><input type='button' class='undoDisabled'/>Undo</span><a href="javascript:undo()" id='undoEnable'><input type='button' class='undo'>Undo</a> <span style="font-weight:normal"> | <span id='redoDisable'><input type='button' class='redoDisabled'>Redo</span><a id='redoEnable' href="javascript:redo()"><input type='button' class='redo'>Redo</a>
					</td>
				</tr>
				<tr align="center">
					<td colspan="2" class="editAreaBg" style="padding:5px;">
						<table width="50%" cellpadding="0" cellspacing="0">
							<tr><td id="previewTableContainerRow"></td></tr>
							<script>
							initCPSLayout(daob, 'previewTableContainerRow','<%=IAMEncoder.encodeJavaScript(layoutType)%>','<%=IAMEncoder.encodeJavaScript(request.getParameter("FormType"))%>','<%=IAMEncoder.encodeJavaScript(request.getParameter("MDS"))%>');
							</script>
						</table>
					</td>
				</tr>
			</table>
			<form name="ViewCreationPropsForm">
				<input type="hidden" value="" name="xmlData">
				<input type="hidden" name="viewtitle" value='<%=IAMEncoder.encodeHTMLAttribute(request.getParameter("VIEWNAME"))%>'>
				<table width="100%" border="0" cellpadding="0" cellspacing="2" class="editAreaBorder" id="COLUMN_PROPERTIES">
					<tr>
						<td class="tableHeader" width='75%'>
							Column Properties<span id="selCol"></span>
						</td>
						<td class="tableHeader" align="right">
							<span id="linkProps"><a href="javascript:showLinkProps();">Show Link Properties</a></span>&nbsp;&nbsp; | 
						</td>
						<td class="tableHeader" align="right">
							<span id="advProps"><a href="javascript:showAdvancedProps();">Show Advanced Properties</a></span>
						</td>
					</tr>
					<tr>
						<td style="height:120" valign='top' colspan="3">
							<%
							if(layoutType.equals("Table")){
								%>
								<%@ include file='TableColumnProperties.jspf'%>
								<%
							}
							else if(layoutType.equals("CPS")){
								%>
								<%@ include file='PSColumnProperties.jspf'%>
								<%
							}
							else if(layoutType.equals("CPSForm")){
								%>
								<%@ include file='FormColumnProperties.jspf'%>
								<%
							}
							%>
						</td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="2" cellspacing="2" class="btnPanel">
					<tr>
						<td align="center" nowrap>&nbsp;
							<%
							if(request.getParameter("FormType") != null){
								%>
								<input name="FormType" value='<%=IAMEncoder.encodeHTMLAttribute(request.getParameter("FormType"))%>' type="hidden"/>
								<%
							}
							if(request.getParameter("VIEWNAME") != null){
								%>
								<input name="VIEWNAME" value='<%=IAMEncoder.encodeHTMLAttribute(request.getParameter("VIEWNAME"))%>' type="hidden"/>
								<%
							}
							else {
								String[] tableNames = request.getParameterValues("selectedTables");
								if(tableNames != null){
								%>
									<select name='selectedTables' class='hide' multiple>	
										<%
										for(int i=0;i<tableNames.length;i++){
											%>
											<option value='<%=IAMEncoder.encodeHTMLAttribute(tableNames[i])%>' selected><%=IAMEncoder.encodeHTML(tableNames[i])%></option>
											<%
										}
										%>
									</select>
									<%
								}
							}
							%>
							<input name="Submit" type="button" class='btn' value="Save" onclick="this.form.submitted = true;fetchViewName();populateRequiredRows(); handleSubmit(); return false;">
							<!--input name="Submit" type="button" value="Save" onclick="fetchViewName(); populateRequiredRows(); return false;"-->
							<input name="Submit2" type="button" value="Cancel" class='btn' onClick="closeView('<%=uniqueId%>');"><%--NO OUTPUTENCODING --%>        
						</td>
					</tr>
				</table>
			</form>
				
		</td>
		<td id='_HIDEPANE' class="paneBg show" rowspan="2"><input type='button' class='hideSideBar' onClick='hidePane()'></td>
		<td id='_SHOWPANE' class="paneBg hide" rowspan="2"><input type='button' class='showSideBar' onclick='showPane()'></td>
		<td id = '_PANE' width="225" valign="top" class="rightBg">
			<%@ include file='../cvtab/viewcreation/RightBar.jspf'%>
		</td>
	</tr>
</table>
<%
if(!layoutType.equals("CPSForm")){
	%>
	<div id="completeDiv" style="border: 1px solid #000000;zIndex:1;padding: 0px; visibility: hidden; position: absolute; background-color: #FFFFFF"></div>
		
	<%
}
%>
<div id="EditTableProps" class="hide">
<table width="400px"  align="center" border="0" cellpadding="2" cellspacing="0" id="propsTable2">
	<tr>
		<td class="confContent ">Column Chooser MenuItem</td>
		<td >
			<select name="COLUMNCHOOSERMENUITEM" id="COLUMNCHOOSERMENUITEM" class="txtField" onChange="updatePropsForTable(this)">
				<option value="CCListInline" selected>List Type - Inline</option> 
				<option value="CCList" >List Type</option>
				<option value="CCSelecTinline">Select Type - Inline</option>
				<option value="CCSelect">Select Type</option>
			</select>
		</td>
	</tr>
	<tr>
		<td nowrap class="confContent ">Navigation Configuration</td>
		<td><components:DropDown configName="NAVIGATIONCONFIG"/></td>
	</tr>
	<tr>
		<td nowrap class="confContent ">Empty Table Message </td>
		<td ><input id="EMPTY_TABLE_MESSAGE" name="EMPTY_TABLE_MESSAGE" type="text" class="txtField" size="30" onkeyup="updatePropsForTable(this)"></td>
	</tr>
	<tr>
		<td nowrap class="confContent ">Page Length </td>
		<td ><input id="PAGELENGTH" name="PAGELENGTH" type="text" class="txtField" size="30" onkeyup="updatePropsForTable(this)"></td>
	</tr>
	<tr>
		<td nowrap class="confContent ">Sort Column </td>
		<td ><input id="SORTCOLUMN" name="SORTCOLUMN" type="text" class="txtField" size="30" onkeyup="updatePropsForTable(this)"></td>
	</tr>
	<tr>
		<td nowrap class="confContent ">Sort Order </td>
		<td >
			<select name="SORTORDER" id="SORTORDER" class="txtField" onChange="updatePropsForTable(this)">
				<option value="ASC" selected>Ascendig</option> 
				<option value="DESC" >Descending</option>
			</select>
		</td>
	</tr>
	<tr>
		<td nowrap class="confContent ">Enable Row Hover </td>
		<td >
			<select name="ENABLEROWHOVER" id="ENABLEROWHOVER" class="txtField" onChange="updatePropsForTable(this)">
				<option value="false" selected>Disable</option> 
				<option value="true" >Enable</option>
			</select>
		</td>
	</tr>
	<tr>
		<td nowrap class="confContent ">Enable Row Selection </td>
		<td >
			<select name="ENABLEROWSELECTION" id="ENABLEROWSELECTION" class="txtField" onChange="updatePropsForTable(this)">
				<option value="NONE" selected>None</option> 
				<option value="CHECKBOX" >CheckBox</option>
				<option value="RADIO" >Radio</option>
			</select>
		</td>
	</tr>
	<tr>
		<td nowrap class="confContent ">Render Menu </td>
		<td >
			<select name="RENDERMENU" id="RENDERMENU" class="txtField" onChange="updatePropsForTable(this)">
				<option value="false" selected>Disable</option> 
				<option value="true" >Enable</option>
			</select>
		</td>
	</tr>
</table>
</div>

