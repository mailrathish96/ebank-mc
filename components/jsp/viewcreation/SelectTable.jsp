<%-- $Id$ --%>
<%@ include file='/components/jsp/CommonIncludes.jspf'%>
<%@ page import = "com.adventnet.persistence.*,com.adventnet.customview.*,com.adventnet.client.util.LookUpUtil,com.adventnet.ds.query.util.*,com.adventnet.ds.query.*,com.adventnet.client.components.util.web.PersonalizationUtil"%>
<%@page import="com.adventnet.iam.xss.IAMEncoder"%>
<script src='<%=request.getContextPath()%>/components/javascript/tableSelection.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<%
String cvName = null;
if(request.getParameter("VIEWNAME") != null){
	String viewName = request.getParameter("VIEWNAME");
	ViewContext viewCtx = ViewContext.getViewContext(viewName, viewName, request);
	DataObject dataObject = viewCtx.getModel().getViewConfiguration();
	if(!PersonalizationUtil.isPlaceHolderView(dataObject)) { 
		if(dataObject.containsTable("ACTableViewConfig")){
			cvName = (String) dataObject.getFirstValue("ACTableViewConfig","CVNAME");
		}
		else if(dataObject.containsTable("ACFormConfig")){
			cvName = (String) dataObject.getFirstValue("ACFormConfig", "CVNAME");
		}
		Row customViewRow = new Row(CUSTOMVIEWCONFIGURATION.TABLE);
		customViewRow.set(CUSTOMVIEWCONFIGURATION.CVNAME_IDX,cvName);
		DataObject customViewDO = LookUpUtil.getPersistence().get(CUSTOMVIEWCONFIGURATION.TABLE, customViewRow);
		long queryID = ((Long)customViewDO.getFirstValue(CUSTOMVIEWCONFIGURATION.TABLE, CUSTOMVIEWCONFIGURATION.QUERYID_IDX)).longValue();
		SelectQuery query = QueryUtil.getSelectQuery(queryID);
		java.util.List list = query.getTableList();
		%><script><%
		for(int i=0; i< list.size();i++){
			String tableName = ((Table)list.get(i)).getTableName();
			%>tableNames[<%=i%>] = '<%=tableName%>';<%--NO OUTPUTENCODING --%>
			<%
		}
		%></script><%
	}
}
%>
<Table width='80%' cellspacing="0" align="center">
	<Tr>
		<Td colspan="2"><table><tr><td>Tables for Application : 
		<% if(request.getParameter("MDS") != null && request.getParameter("MDS").equals("true")){
			%><components:DropDown configName="MDSApplicationDropDown"/><%
		} else {
			%><components:DropDown configName="ApplicationDropDown"/><%
		} %>
			</Td>
			<td><form><input type="checkbox" name="MDS" value="true" onChange="this.form.submit();"/> &nbsp;&nbsp;Use Multiple Data Source</form></td>
			</tr></table>
		</td>
	</Tr>
	<Tr>
		<Td valign="top" width="50%" style="padding:5px";>
		     <client:showView viewName="TableNamesView"/>
		</Td>
		<Td valign="top" style="padding:5px;">
			<client:box boxType="PrimaryBox" boxId='SList' title="Selected Tables">
				<div id='SelectedTablesView'> </div>
			</client:box>
		</Td>
	</Tr>
</Table>
<%
String mds = request.getParameter("MDS");
%>
<Form name="SC">
	<Table width='100%'>
		<tr>
			<td align='center'>
				<Select name="selectedTables" class='hide' multiple></Select>
			<%
                           if(mds != null){
				   %>
				<input name="MDS" value='' type="hidden"/>
					   <%
			   }
                           if(request.getParameter("VIEWNAME") != null){
			%>
				<input name="VIEWNAME" value='<%=IAMEncoder.encodeHTMLAttribute(request.getParameter("VIEWNAME"))%>' type="hidden"/><%--NO OUTPUTENCODING --%>
			<%
        			}
			%>
				<input type="hidden" name="LayoutType" value="<%=viewContext.getModel().getFeatureValue("LayoutType")%>"/><%--NO OUTPUTENCODING --%>
				<input type="button" value="Auto Join" class='btn' onClick="return selectTablesAndSubmit('<%=IAMEncoder.encodeJavaScript(mds)%>','AUTO')"/>
				<input type="button" value="Manual Join" class='btn' onClick="return selectTablesAndSubmit('<%=IAMEncoder.encodeJavaScript(mds)%>','MANUAL')"/>
				<input name="Submit2" type="button" value="Cancel" class='btn' onClick="closeView('<%=uniqueId%>');"><%--NO OUTPUTENCODING --%>        
			</td>	
		</tr>	
	</table>
	<br>
</Form>
<Script>
	var uniqueId = "TableNamesView";
	var elements = document.getElementsByTagName("select");
	for(var i = 0; i < elements.length;i++){
		if(elements[i].name == "ApplicationDropDown" || elements[i].name == "MDSApplicationDropDown"){
			updateState(uniqueId, "_D_RP", "&APP_ID=" + elements[i].value);
		}
	}
	refreshSubView(uniqueId);
	constructTableList();
</Script>

