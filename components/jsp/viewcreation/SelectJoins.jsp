<%-- $Id$ --%>
<%@ include file='/components/jsp/CommonIncludes.jspf'%>
<%@ page import = "com.adventnet.persistence.*,com.adventnet.customview.*,com.adventnet.client.util.LookUpUtil,com.adventnet.ds.query.util.*,com.adventnet.ds.query.*,com.adventnet.client.components.util.web.PersonalizationUtil"%>
<%@page import="com.adventnet.iam.xss.IAMEncoder"%>
<% String path = request.getContextPath(); String comppath = path + "/components/javascript/"; String vcpath = comppath + "viewcreation/"; String layoutType = (String)request.getAttribute("LayoutType"); if(layoutType == null){ layoutType = request.getParameter("LayoutType"); if(layoutType == null){ layoutType = "CPSForm"; } }%>
<style>
select {
width:175;
}
</style>
<script src='<%=vcpath%>DO.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=vcpath%>CreateViewForm.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<%=request.getAttribute("JSCODE")%><%--NO OUTPUTENCODING --%>
<% String[] tableNames = request.getParameterValues("selectedTables");%>
<script>var tableNames = new Array();
<% for(int i=0; i<tableNames.length; i++){%>
tableNames[<%=i%>] = '<%=IAMEncoder.encodeHTMLAttribute(tableNames[i])%>';
<% }%></script>
<br>
<table border="0" cellspacing='1' cellpadding="0" align="center" class="formTable" style="width:95%">
<%
if(request.getParameter("JOINTYPE").equals("MANUAL")){
%>
  <tr> 
    <td colspan="2" class="LabelClass"> Join Editor</td>
  </tr>
  <tr> 
    <td nowrap class="FieldNameClass" style="background-image:none;text-align:left;padding:0px"> <table border="0" cellspacing='2' cellpadding='2'>
        <tr> 
          <td height="20">Main Table:</td>
          <td> <select name="MainTable">
              <option value="NONE"> -------------------- </option>
              <% for(int i=0; i<tableNames.length; i++){%>
              <option value='<%=IAMEncoder.encodeHTMLAttribute(tableNames[i])%>'><%=IAMEncoder.encodeHTML(tableNames[i])%></option>
              <% }%>
            </select> </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td class="FieldValueClass" style="background:#FFF;text-align:left;padding:5px 0px 0px 0px"> 
      <% for(int count = 0; count < tableNames.length - 1; count++){%>
      <div id="JoinCode"> 
        <table border="0" id="JoinTable<%=count%>" cellspacing='2' cellpadding='1'><%--NO OUTPUTENCODING --%>
          <tr> 
            <td nowrap>Child Table:</td>
            <td> <select name="BaseTable<%=count%>" id="BaseTable<%=count%>"><%--NO OUTPUTENCODING --%>
                <option value="NONE"> -------------------- </option>
                <% for(int i=0; i<tableNames.length; i++){%>
                <option value='<%=IAMEncoder.encodeHTMLAttribute(tableNames[i])%>'><%=IAMEncoder.encodeHTML(tableNames[i])%></option>
                <% }%>
              </select> </td>
            <td nowrap>Join Type:</td>
            <td> <select name="Join<%=count%>" id="Join<%=count%>"><%--NO OUTPUTENCODING --%>
                <option value="1">Left Join</option>
                <option value="2">Inner Join</option>
              </select> </td>
            <td nowrap>Referenced Table:</td>
            <td> <select name="ReferenceTable<%=count%>" id="ReferenceTable<%=count%>"><%--NO OUTPUTENCODING --%>
                <option value="NONE"> -------------------- </option>
                <% for(int i=0; i<tableNames.length; i++){%>
                <option value='<%=IAMEncoder.encodeHTMLAttribute(tableNames[i])%>'><%=IAMEncoder.encodeHTML(tableNames[i])%></option>
                <% }%>
              </select> </td>
            <td> <button class='btn' onClick="return addJoinColumn(<%=count%>)">Add Column </button>&nbsp;<%--NO OUTPUTENCODING --%>
			  <button class='btn' onClick="return deleteJoinColumns(<%=count%>)">Reset</button></td>
          </tr>
          <tr> 
            <td style='padding:5px 0px 5px 0px;' colspan="8" id = "JoinColumns<%=count%>"><%--NO OUTPUTENCODING --%>
			</td>
          </tr>
        </table>
      </div>
      <script>var dropDownForTable = new Object();
var queryId = null;
function getQueryId(){
var queryRow = daob.getRowsForTable("SelectQuery")[0];
queryId = queryRow["QUERYID"];
}
function addSelectTables(){
for(var i=0; i<tableNames.length; i++){
var row = daob.getFirstRow("SelectTable","TABLEALIAS",tableNames[i]);
if(row == null){
daob.addRowsForTable("SelectTable", new Array(new Array("QUERYID", queryId), new Array("TABLENAME", tableNames[i]), new Array("TABLEALIAS", tableNames[i])));
}
}
}
function constructDropDown(){
var selectTables = daob.getRowsForTable("SelectTable");
for(var index in selectTables){
var selectColumns = daob.getRows("SelectColumn","TABLEALIAS",selectTables[index]["TABLEALIAS"]);
var reqCode = "<option value='NONE'>--------------------</option>";
for(var count =0; count < selectColumns.length; count++){
reqCode = reqCode.concat("<option value='" + selectColumns[count]["COLUMNNAME"] + "'>" + selectColumns[count]["COLUMNNAME"] + "</option>");
}
dropDownForTable[selectTables[index]["TABLEALIAS"]] = reqCode;
}
}
function addJoinColumn(index){
	var childTable = document.getElementById("BaseTable" + index).value;
	var refTable = document.getElementById("ReferenceTable" + index).value;
	if(childTable == "NONE" || refTable == "NONE"){
		alert("Error!!!  Tables not properly selected ");
		return false;
	}
	if(refTable == childTable){
		alert("Error!!! Child and Reference table's cannot be the same.");
		return false;
	}
	var joinType = document.getElementById("Join" + index).value;
	document.getElementById("BaseTable" + index).disabled = true;
	document.getElementById("ReferenceTable" + index).disabled = true;
	document.getElementById("Join" + index).disabled = true;
	var tableRow = daob.getFirstRow("JoinTable","TABLEALIAS",childTable);
	if(tableRow == null){
		daob.addRowsForTable("JoinTable", new Array(new Array("QUERYID", queryId), new Array("TABLEALIAS", childTable), new Array("REFERENCEDTABLE", refTable), new Array("JOINTYPE", joinType)));
	}
	daob.addRowsForTable("JoinColumns", new Array(new Array("QUERYID", queryId), new Array("TABLEALIAS", childTable), new Array("BASETABLECOLUMN", "NONE"), new Array("REFERENCEDTABLECOLUMN", "NONE")));
	updateJoinColumns(index, childTable, refTable);
	return false;
}

function deleteJoinColumns(index){
var childTable = document.getElementById("BaseTable" + index).value;
var refTable = document.getElementById("ReferenceTable" + index).value;
document.getElementById("BaseTable" + index).disabled = false;
document.getElementById("ReferenceTable" + index).disabled = false;
document.getElementById("Join" + index).disabled = false;
daob.deleteAllRowsInTable("JoinColumns","TABLEALIAS", childTable);
daob.deleteAllRowsInTable("JoinTable","TABLEALIAS", childTable);
updateJoinColumns(index, childTable, refTable);
return false;
}
function updateJoinColumns(index, childTable, refTable){
var joinColumns = daob.getRows("JoinColumns", "TABLEALIAS", childTable);
var completeCode = "<Table>";
for(var i = 0; i < joinColumns.length; i++){
var htmlCode = document.getElementById("JoinColumn").innerHTML;
var newCode = htmlCode.replace(/\\$IDX/g, i);
newCode = newCode.replace(/\\$ID/g, index);
var refSelect = "<select id = 'RefTableColumn_" + index + "_" + i + "' onChange='updateJoinColumnValues("+ index + "," + i + ")'>" + dropDownForTable[childTable] + "</select>";
var baseSelect = "<select id = 'BaseTableColumn_" + index + "_" + i + "' onChange='updateJoinColumnValues("+index + "," + i + ")'>" + dropDownForTable[refTable] + "</select>";
newCode = newCode.replace(/\\$OPTIONS/g, refSelect);
newCode = newCode.replace(/\\$BASEOPTIONS/g, baseSelect);
completeCode = completeCode.concat(newCode);
}
completeCode = completeCode.concat("</Table>");
document.getElementById("JoinColumns" + index).innerHTML = completeCode;
for(var j = 0 ; j < joinColumns.length; j++){
document.getElementById("RefTableColumn_" + index + "_" + j).value = joinColumns[j]["REFERENCEDTABLECOLUMN"];
document.getElementById("BaseTableColumn_" + index + "_" + j).value = joinColumns[j]["BASETABLECOLUMN"];
}
}
function updateJoinColumnValues(tableIdx, joinIdx){
var childTable = document.getElementById("BaseTable" + tableIdx).value;
var tableRow = daob.getRows("JoinColumns","TABLEALIAS",childTable);
var reqRow = tableRow[joinIdx];
reqRow["REFERENCEDTABLECOLUMN"] = document.getElementById("RefTableColumn_" + tableIdx + "_" + joinIdx).value;
reqRow["BASETABLECOLUMN"] = document.getElementById("BaseTableColumn_" + tableIdx + "_" + joinIdx).value;
}
function deleteJoinColumn(tableIdx, joinIdx){
var childTable = document.getElementById("BaseTable" + tableIdx).value;
var tableRow = daob.getRows("JoinColumns","TABLEALIAS",childTable);
var reqRow = tableRow[joinIdx];
var baseColumn = reqRow["BASETABLECOLUMN"];
daob.deleteFirstRow("JoinColumns", "BASETABLECOLUMN", baseColumn);
var childTable = document.getElementById("BaseTable" + tableIdx).value;
var refTable = document.getElementById("ReferenceTable" + tableIdx).value;
updateJoinColumns(tableIdx, childTable, refTable);
}
getQueryId();
addSelectTables();
constructDropDown();</script> <div id="JoinColumn" class='hide'> 	
        <table border="0" cellspacing='1' cellpadding='4' class="formTable" style="width:auto">
          <tr class="FieldValueClass" style="background-image:none"> 
            <td nowrap> Child Table Column: </td>
            <td>$OPTIONS</td>
            <td nowrap> Base Table Column: </td>
            <td>$BASEOPTIONS</td>
            <td> <button class='btn' onClick = "deleteJoinColumn($ID, $IDX)">Delete</button></td>
          </tr>
        </table>
      </div></td>	  
  </tr>
  <%
  }
}
%>
	<tr>
	<td>
	<br>Note : <ul><li>Select the columns for which the data should be fetched.</li>
                       <ul><li>This includes the columns that you might need for passing it as parameters to links.</li></ul>
                       <li>The details of whether the column should be shown by default or not can be configured in the Table Construction screen</li></ul>
  <form name="SC" action="SelectJoins.ve" method="post">
	<div id="showSelectColumns"></div>
	<script>
	function constructSelectColumns(){
		var columns = daob.getRowsForTable("SelectColumn");
		var code = "<table width='100%' id='ColumnList' cellspacing='0' cellpadding='0'>";
	      	code = code.concat("<tr><th colspan='3' class='LabelClass'>Select Columns</th></tr>");

		code = code.concat("<tr><th class='tableHeader'>&nbsp;</th><th class='tableHeader'>Column Name</th><th class='tableHeader'>Table Name</th></tr>");
		for(var col=0; col < columns.length; col++){
			column = columns[col];
			code = code.concat("<tr><td width='15px'><input type='checkbox' checked value='"+column["COLUMNALIAS"]+"'></td>");
			code = code.concat("<td>" + column["COLUMNNAME"] + "</td>");
			code = code.concat("<td>" + column["TABLEALIAS"] + "</td></tr>");
		}
		code = code.concat("</table>");
		document.getElementById("showSelectColumns").innerHTML = code;
		alternateRows("ColumnList", "evenRow","oddRow");
	}

	function selectJoinsAndSubmit(mds){
	var selectedTablesParam = "";
	var elements = document.SC.elements;
	for(var i=0; i<elements.length;i++){
		if(elements[i].type == "checkbox"){
			if(elements[i].checked != true){
				daob.deleteFirstRow("SelectColumn","COLUMNALIAS", elements[i].value);
			}
		}
	}
	document.SC.SQLQUERYasXML.value = daob.constructXML();
	document.SC.submit();
}

	constructSelectColumns();
	</script>
        <% String mds = request.getParameter("MDS"); if(mds != null){%>
        <input name="MDS" value='<%=IAMEncoder.encodeHTMLAttribute(mds)%>' type="hidden"/>
        <% } if(request.getParameter("VIEWNAME") != null){%>
        <input name="VIEWNAME" value='<%=IAMEncoder.encodeHTMLAttribute(request.getParameter("VIEWNAME"))%>' type="hidden"/>
        <% }%>
        <select name="selectedTables" multiple class='hide'>
          <% for(int i=0; i<tableNames.length; i++){%>
          <option value='<%=IAMEncoder.encodeHTMLAttribute(tableNames[i])%>' selected><%=IAMEncoder.encodeHTML(tableNames[i])%></option>
          <% }%>
        </select>
        <input type="hidden" name="LayoutType" value="<%=IAMEncoder.encodeHTMLAttribute(layoutType)%>"/>
        <input type="hidden" name="JOINTYPE" value="MANUAL"/>
        <input type="hidden" name="SQLQUERYasXML" value=""/>
        <div align='center' class="ButtonPanelClass"> 
          <input type="button" onClick="handleStateForForm(this.form); selectJoinsAndSubmit('<%=IAMEncoder.encodeJavaScript(mds)%>')" value='Next' class='btn'>
        </div>
      </form>
</td>
</tr>
</table>
</div>
