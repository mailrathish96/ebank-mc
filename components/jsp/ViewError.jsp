<%-- $Id$ --%>
<%boolean isXMLHTTP = com.adventnet.client.view.web.WebViewAPI.isAjaxRequest(request);%>

<%@ page import="org.apache.struts.*,org.apache.struts.action.*,java.util.*,org.apache.struts.util.*" %>
<%@ page import="java.io.*,javax.servlet.*" isErrorPage="true" %>
<%@page import="com.adventnet.client.themes.web.ThemesAPI"%>
<%@page import="com.adventnet.client.util.web.ExceptionUtils"%>
<%@page import="com.adventnet.client.tpl.TemplateAPI"%>
<%
try{
	String themesDir = ThemesAPI.getThemeDirForRequest(request);
if(!isXMLHTTP){

%>
<html>
<head>
   <link href="<%=themesDir%>/styles/style.css" rel="stylesheet" type="text/css"/><%--NO OUTPUTENCODING --%>
   <script src='<%=request.getContextPath()%>/framework/javascript/IncludeJS.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
   <script>includeMainScripts('<%=request.getContextPath()%>');</script><%--NO OUTPUTENCODING --%>
</head>
<%}%>
 <SCRIPT src='<%=request.getContextPath()%>/components/javascript/error.js'></SCRIPT><%--NO OUTPUTENCODING --%>
<%if(isXMLHTTP){%>
<div part="RESPONSE_STATUS" style="display:none">__ERROR__PAGE</div>
<div part="STATUS_MESSAGE">
An error when processing the request.... <a
href="javascript:showCompError();">Click Here</a> for more Details
<div id="completeErrorDiv" class="hide">	
<%
out.println(TemplateAPI.givehtml("DialogBox1_Prefix",null,new Object[][]{{"TITLE","Error"}}));//NO OUTPUTENCODING%>
<%}%>
		<!-- start template -->	
		<%
		out.println(TemplateAPI.givehtml("ErrorDialog_Prefix",null,new Object[][]{}));//NO OUTPUTENCODING%>
       <div id="StackTrace" style="overflow:auto;">
         <table>
          <% initErrorListing(request,response,exception); %>
          <% while(base != null){ %>
        <tr>
           <td>
              caused by
           </td>
        </tr>
        <tr> 
           <td>
             <a href="javascript:hideShowError('<%=counter%>')"><%--NO OUTPUTENCODING --%>
                 <img id="bullet<%=counter%>" width="8" height="8" border="0" src="<%=request.getContextPath()%>/themes/common/images/arrow_up.png"></img><%--NO OUTPUTENCODING --%>
                 <%=base.toString()%><%--NO OUTPUTENCODING --%>
             </a>
           </td>
           </tr>
           <tr  id="exception<%=counter%>" class="hide"><%--NO OUTPUTENCODING --%>
             <td>
                <%= ExceptionUtils.getStackTrace(base)%><%--NO OUTPUTENCODING --%>
             </td>
           </tr>
           
        <% nextException();}%>
         
         </table>
        </div>
        <%
		out.println(TemplateAPI.givehtml("ErrorDialog_Suffix",null,new Object[][]{}));//NO OUTPUTENCODING
        %>
       
		<!--  end template -->

<%if(isXMLHTTP){
	out.println(TemplateAPI.givehtml("DialogBox1_Suffix",null,new Object[][]{}));//NO OUTPUTENCODING
%>
</div>
</div>
<%}%>

<%
}
catch(Exception ex){
	ex.printStackTrace();
}
%>


<%!
  Throwable base;
  int counter=0;
  public void initErrorListing(HttpServletRequest request,HttpServletResponse
  resp,Throwable exception)
  {
       base = exception;
       if(base == null)
       {
           base  = (Throwable)request.getAttribute(Globals.EXCEPTION_KEY);
       } 
  }
  

  public void nextException()
  {
    base = ExceptionUtils.getCause(base);        
    counter++;
  }

%>
