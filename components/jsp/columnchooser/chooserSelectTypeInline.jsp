<%-- $Id$ --%>
<%@ include file='../CommonIncludes.jspf'%>
<%@ page import = "com.adventnet.client.components.table.web.*, com.adventnet.client.util.web.*,java.util.*"%>
<%@page import="com.adventnet.client.util.web.WebClientUtil"%>
<%@page import="com.adventnet.persistence.*"%>
<%@page import="com.adventnet.i18n.I18N"%>
<%@page import="com.adventnet.iam.xss.IAMEncoder"%>
<%@ page isELIgnored="false" %>
<%
	String tableViewId = WebClientUtil.getXSSSafeParameter("TABLEVIEWID", request);
%>
<html>
<head>
<title><%=I18N.getMsg("mc.components.Table_Column_Customization")%></title><%--NO OUTPUTENCODING --%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<%
if(request.getParameter("View_Name") != null){
	TablePersonalizationUtil.updateColumnsListForView(request.getParameter("View_Name"), WebClientUtil.getAccountId(), request.getParameterValues("SHOW_LIST"), request.getParameterValues("HIDE_LIST"));
}
else {
 
ArrayList showList = new ArrayList();
ArrayList hideList = new ArrayList();
TablePersonalizationUtil.setColumnsListForView(request.getParameter("viewName"), WebClientUtil.getAccountId(), showList, hideList, request);
%>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" cellpadding="0" cellspacing="0" class="productLogo" height="30">
  <tr> 
    <td class="titleText"><%=I18N.getMsg("mc.components.Table_Column_Customization")%></td><%--NO OUTPUTENCODING --%>
  </tr>
</table>
<div id="chooserTable">
  <form name="tableview" method="post">
    <table border="0" class="ccListTableInline" cellpadding="7" cellspacing="0">
	  <tr> 	  
	    <td> 		
		<input type = "hidden" name = "View_Name" value = '<%=WebClientUtil.getXSSSafeParameter("viewName", request)%>'><%--NO OUTPUTENCODING --%>
          <input type = "hidden" name = "TABLEVIEWID" value = '<%=IAMEncoder.encodeHTMLAttribute(tableViewId)%>'> <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr> 
              <td class="bodyText" nowrap><%=I18N.getMsg("mc.components.Non_Visible_Columns")%>: <br> <select name="HIDE_LIST" size="8" multiple class="listStyle "><%--NO OUTPUTENCODING --%>
                  <%
								int hideSize = hideList.size();
								for(int i = 0; i<hideSize; i++){
									String[] values = (String[]) hideList.get(i);
									%>
                  <option value="<%=IAMEncoder.encodeHTMLAttribute(values[0])%>"><%=I18N.getMsg(IAMEncoder.encodeHTMLAttribute(values[1]))%></option><%--NO OUTPUTENCODING --%>
                  <%	
								}
							%>
                </select> </td>
              <td valign="middle" width="10%" nowrap> <table width="100%" border="0" cellspacing="2" cellpadding="4">
                  <tr> 
                    <td align="center" nowrap><input type="button" onClick='return updateLists(document.forms["tableview"].HIDE_LIST,document.forms["tableview"].SHOW_LIST)' class="add"/></td>
                  </tr>
                  <tr> 
                    <td align="center" nowrap><input type="button" onClick='return moveAll(document.forms["tableview"].HIDE_LIST,document.forms["tableview"].SHOW_LIST)' class="addAll"/></td>
                  </tr>
                  <tr> 
                    <td align="center" nowrap><input type="button" onClick='return updateLists(document.forms["tableview"].SHOW_LIST,document.forms["tableview"].HIDE_LIST)' class="removeColumn"/></td>
                  </tr>
                  <tr> 
                    <td align="center" nowrap><input type="button" onClick='return moveAll(document.forms["tableview"].SHOW_LIST,document.forms["tableview"].HIDE_LIST)' class="removeAll"/></td>
                  </tr>
                </table></td>
              <td class="bodyText" nowrap> <%=I18N.getMsg("mc.components.Visible_Columns")%>: <br> <select name="SHOW_LIST" multiple class="listStyle" size="8"><%--NO OUTPUTENCODING --%>
                  <%
					int showSize = showList.size();
					for(int i = 0; i<showSize; i++){
						String[] values = (String[]) showList.get(i);
						%>
                  <option value="<%=IAMEncoder.encodeHTMLAttribute(values[0])%>"><%=I18N.getMsg(IAMEncoder.encodeHTMLAttribute(values[1]))%></option><%--NO OUTPUTENCODING --%>
                  <%
					}
				%>
                </select> </td>
              <td width="10%" nowrap> <table width="100%" border="0" cellspacing="1" cellpadding="5">
                  <tr> 
                    <td align="center" nowrap><input type="button" onClick='return moveUp(document.forms["tableview"].SHOW_LIST)' class="moveUp"/></td>
                  </tr>
                  <tr> 
                    <td align="center" nowrap><input type="button" onClick='return moveDown(document.forms["tableview"].SHOW_LIST)' class="moveDown"/></td>
                  </tr>
                </table></td>
            </tr>
          </table></td>
      </tr>
	</table>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	  <tr> 
        <td class="ccBtnPanel" nowrap> <input name="Submit" type="button" class="btn" value='<%=I18N.getMsg("mc.components.Save")%>' onClick="return submitCCSelectForm(this.form);return false;"><%--NO OUTPUTENCODING --%>
          <input name="Submit2" type="button" class="btn" value='<%=I18N.getMsg("mc.components.Cancel")%>' onClick="closeDialog();"><%--NO OUTPUTENCODING --%>
        </td>
      </tr>
    </table>
    <input type="hidden" name="USER_VAR" value="${param.USER_VARIABLE}" /><%--NO OUTPUTENCODING --%>
	</form>
  </div>
<%
}
%>
</body>
</html>
