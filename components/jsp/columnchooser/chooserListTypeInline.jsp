<%-- $Id$ --%>
<%@ include file='../CommonIncludes.jspf'%>
<%@ page import = "com.adventnet.client.components.table.web.*, com.adventnet.client.util.web.*,java.util.*"%>
<%@page import="com.adventnet.client.util.web.WebClientUtil"%>
<%@page import="com.adventnet.persistence.*"%>
<%@page import="com.adventnet.i18n.I18N"%>
<%@page import="com.adventnet.iam.xss.IAMEncoder"%>
<%@ page isELIgnored="false" %>
<%
	String tableViewId = WebClientUtil.getXSSSafeParameter("TABLEVIEWID", request);

if(request.getParameter("View_Name") != null){
	TablePersonalizationUtil.updateColumnsListForView(request.getParameter("View_Name"), WebClientUtil.getAccountId(), request.getParameterValues("SHOW_LIST"), request.getParameterValues("HIDE_LIST"));
}
else {
	ArrayList showList = new ArrayList();
	ArrayList hideList = new ArrayList();
	TablePersonalizationUtil.setColumnsListForView(request.getParameter("viewName"), WebClientUtil.getAccountId(), showList, hideList, request);
%></head><div id="chooserTable">
<form name="tableview" method="post">
  <input type = "hidden" name = "View_Name" value = '<%=WebClientUtil.getXSSSafeParameter("viewName", request)%>'><%--NO OUTPUTENCODING --%>
  <input type = "hidden" name = "TABLEVIEWID" value = '<%=tableViewId%>'><%--NO OUTPUTENCODING --%>
  <table border="0" align="center" cellpadding="0" cellspacing="0" class="ccListTableInline">
    <tr> 
      <td width="80%"> <div id='ccTable'></div></td>
      <td class="ccMovePanel"> <table width="100%" border="0" cellspacing="1" cellpadding="5">
          <tr> 
            <td align="center"><button onClick="return moveColumnUp()" value="^" class="moveUp" title="Move Up"></button></td>
          </tr>
          <tr> 
            <td align="center"><button onClick="return moveColumnDown()" value="^" class="moveDown" title="Move Down"></button></td>
          </tr>
        </table></td>
    </tr>
    <script>
	var list = new Array();
	<%
	int count = 0;
	for(int i=0; i<showList.size();i++){
		String[] values = (String[]) showList.get(i);
                values[0] = values[0].replaceAll("'","\\\\'");
                values[1] = values[1].replaceAll("'","\\\\'");
		%>list[<%=i%>] = new Array('<%=IAMEncoder.encodeJavaScript(values[0])%>','<%=I18N.getMsg(IAMEncoder.encodeJavaScript(values[1]))%>','true');<%--NO OUTPUTENCODING --%>
		<%
		count++;
	}
	for(int i=0; i<hideList.size();i++){
		String[] values = (String[]) hideList.get(i);
                values[0] = values[0].replaceAll("'","\\\\'");
                values[1] = values[1].replaceAll("'","\\\\'");
		%>
		list[<%=count%>] = new Array('<%=IAMEncoder.encodeJavaScript(values[0])%>','<%=I18N.getMsg(IAMEncoder.encodeJavaScript(values[1]))%>','false');<%--NO OUTPUTENCODING --%>
		<%
		count++;
	}
	%>
	initColumnChooser(list,document, true);
</script>
    <select name="SHOW_LIST" multiple class="hide">
    </select>
    <select name="HIDE_LIST" multiple class="hide">
    </select>
    <tr> 
      <td colspan="2" class="ccBtnPanel"> <input name="Submit" type="button" class="btn" value='<%=I18N.getMsg("mc.components.Save")%>' onClick="return submitCCListForm(this.form);return false;"><%--NO OUTPUTENCODING --%> 
        <input name="Submit2" type="button" class="btn" value='<%=I18N.getMsg("mc.components.Cancel")%>' onClick="closeDialog(null,this);"> <%--NO OUTPUTENCODING --%>
      </td>
    </tr>
  </table>
</form>
</div> 
<%
}
%></body>
</html> 
