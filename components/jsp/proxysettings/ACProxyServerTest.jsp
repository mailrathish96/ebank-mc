<%--$Id$--%>
  <%@ include file='/components/jsp/CommonIncludes.jspf'%>

<script>
var isLoaded = (window.opener == null || window.opener == 'null') ? true : false;
if(!isLoaded)
{
    var proxyname = window.opener.document.ProxySettingsForm.PROXY_HOST.value;
    var proxyport = window.opener.document.ProxySettingsForm.PROXY_PORT.value;
    var username = window.opener.document.ProxySettingsForm.PROXY_USERNAME.value;
    var password = window.opener.document.ProxySettingsForm.PROXY_PASSWORD.value;

    // this will get the radio button list in a array and will iterate through it to findout
    // value of element selected
    var nodeList = window.opener.document.ProxySettingsForm.DIRECT_CONNECTION_TO_INTERNET;
    var directconnection = '';
      for(i=0;i<nodeList.length;i++)
      {
        if(nodeList[i].checked)
        {
            directconnection = nodeList[i].value;
            // may be we can return
        }
      }
}
</script>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="configTableHdr">&nbsp;<strong>Test Proxy Settings</strong></td>
  </tr>
  <tr> 
    <td> <table width="100%" border="0" cellpadding="10" cellspacing="0">
        <tr> 
          <td align="center" valign="middle"> 
          </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
<form name="ProxyTestActionForm" action="ProxyServerTest.ve" target="formSubmitFrame" method="POST" 
      onsubmit="handleStateForForm(this.form)">

<input type="hidden" name="PROXY_HOST" id="PROXY_HOST">
<input type="hidden" name="PROXY_PORT" id="PROXY_PORT">
<input type="hidden" name="PROXY_USERNAME" id="PROXY_USERNAME">
<input type="hidden" name="PROXY_PASSWORD" id="PROXY_PASSWORD">
<input type="hidden" name="DIRECT_CONNECTION_TO_INTERNET" id="DIRECT_CONNECTION_TO_INTERNET">

<script>

if(!isLoaded)
{
    document.getElementById("PROXY_HOST").value = proxyname;
    document.getElementById("PROXY_PORT").value = proxyport;
    document.getElementById("PROXY_USERNAME").value =username;
    document.getElementById("PROXY_PASSWORD").value = password;
    document.getElementById("DIRECT_CONNECTION_TO_INTERNET").value = directconnection;
    var formObj = eval('document.ProxyTestActionForm');
    handleStateForForm(formObj);
    formObj.submit();
}
</script>


</form>


  

