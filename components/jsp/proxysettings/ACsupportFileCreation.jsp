<%--$Id$--%>
<%@ include file='/components/jsp/CommonIncludes.jspf'%>
<%@page import="java.io.*"%>
<%@page import="com.adventnet.client.util.web.*"%>

<%@page import="com.adventnet.persistence.*"%>
<%@page import="com.adventnet.i18n.I18N"%>
<head>
<script>
function setSendSupport(id,str)
{
 var sendDiv= document.getElementById(id);

// alert(sendDiv);
 sendDiv.className =str;
// sendDiv.className =null;
}

function closeSupportForm()
{
  setSendSupport('closeview','hide');
  setSendSupport('sendsupportfile','hide');
}

function createZip(obj)
{
    obj.disabled = true;
	obj.value='Creating...';
    obj.className = 'btnStyledisabled';
    openURL('ZipCreator.ma?NUM_FILES=3&FILE_NAMES=serverout');
}

function validtextarea()
{
var comment=document.upform.userMessage.value;
if(comment==""){
return false
}
return true
}


</script>
</head>
<%
String uploadFileName = (String)request.getAttribute("FILENAME");
String uploadfile = "";
String home=System.getProperty("server.dir");
String supportEmail = ACClientUtil.getClientProps("SUPPORT_EMAIL");
supportEmail = (supportEmail == null) ? "support@adventnet.com" : supportEmail;
char sep = File.separatorChar;

//String relFilePath = "logs"+sep+"serverout0.txt"+sep;
//String relFilePath = "webapps" + sep + "mc" + sep + "logs" + sep + "supportlogs" + sep;
String relFilePath = request.getRealPath("/logs/supportlogs/");
relFilePath = (uploadFileName != null) ? relFilePath + sep + uploadFileName : "";
int length= home.length();
//sometimes the getRealPath returns a empty string, just to avoid exceptions during
//those cases.
String dispRelPath = (relFilePath.trim().length() > 0) ?relFilePath.substring(length) : "";
//String dispFileName = "<i>&lt;Home&gt;</i>"+sep+ relFilePath;
String dispFileName = "<i>&lt;Home&gt;</i>"+dispRelPath;
//String dispFileName = relFilePath;
//uploadfile=home+sep+relFilePath;
uploadfile=relFilePath;
%>

<table cellspacing="0" cellpadding="5" width="100%" border="0">
  <tr>
    <td>
      <input type="button" id="createButton" style="height:18px; width:70px;" class="button" onClick="createZip(this);" value='<%=I18N.getMsg("mc.components.Create")%>'/><%=I18N.getMsg(" mc.components.proxysettings.the_latest_support_information_file")%><%--a href= "javascript:void(null)" onClick="contextHelp('support');"><img src="<%=request.getContextPath()%>/images/contexthlp.gif" border="0" align="middle"></a--%>   <%--NO OUTPUTENCODING --%> 
    </td>
  </tr>
</table>

<%
if(uploadFileName != null)
{
%>
<table cellspacing="0" cellpadding="5" width="100%" border="0">
  <tr>
    <td>
       <div id="closeview" class="primaryOptionBg" style="padding:3px; border:1px solid #E2EFE2; width:100%">
       <%=I18N.getMsg("	mc.components.proxysettings.The_support_file")%><a class="tophilinks" href="<%=request.getContextPath()%>/logs/supportlogs/<%=uploadFileName%>"><%=uploadFileName%></a><%=I18N.getMsg(" mc.components.proxysettings.has_beeen_created_and_is_available_for_download")%>&nbsp;&nbsp;&nbsp;&nbsp;<input name="Submit" id='upbutton' type="button" style="width:80" class="button" onClick="setSendSupport('sendsupportfile','show');setSendSupport('upbutton','hide');" value='<%=I18N.getMsg("mc.components.proxysettings.Upload")%>'><%--NO OUTPUTENCODING --%> 
       </div>
    </td>
  </tr>
</table>
<%
}
%>

<div id='sendsupportfile' class="hide" style="padding:3px; border:1px solid #E2EFE2; width:98%">
<form   name="upform">
    <table border="0" align='center' cellpadding="3" cellspacing="0" width="100%" class="tableBorder" height="90">
      <tr> 
        <td colspan='2'><%=I18N.getMsg(" mc.components.proxysettings.To_send_the_file_to_Technical_Support_team_provide_details_below_and_click_Send")%></td>  <%--NO OUTPUTENCODING --%>
      </tr>
      <tr colspan=2>
      <td>&nbsp;</td>
      </tr>
      
      <tr> 
        <td align='right' class="textSmall"><%=I18N.getMsg("mc.components.proxysettings.To_:")%></td>  <%--NO OUTPUTENCODING --%>
        <td class="textSmall"><%=supportEmail%></td><%--NO OUTPUTENCODING --%>
      </tr>
      <tr> 
        <td align='right' class="textSmall" ><%=I18N.getMsg("mc.components.proxysettings.Email_Id_:")%></td>  <%--NO OUTPUTENCODING --%>
        <td> <input class="formStyle" name="fromAddress" maxlength="100" size="45" value=""  isnullable="false" validatemethod="isEmailId" errormsg='<%=I18N.getMsg("mc.components.proxysettings.Email_is_not_a_valid_one")%>'> <div id="fromAddress_DIV"></div>  <%--NO OUTPUTENCODING --%>
        </td>
      </tr>
      <tr> 
        <td valign="top" class="textSmall" align='right' nowrap><%=I18N.getMsg("mc.components.proxysettings.Comments_:")%></td>  <%--NO OUTPUTENCODING --%>
        <td height="22" valign="top"> 
		<textarea name="userMessage" class="formStyle" wrap="soft" rows="7" cols="43" isnullable="false" validatemethod="validtextarea"  errormsg='<%=I18N.getMsg("mc.components.proxysettings.Please_enter_the_comments")%>'></textarea><div id="userMessage_DIV"></div>  <%--NO OUTPUTENCODING --%>
        </td>
      </tr>
      <tr> 
        <td align='right' class="textSmall">&nbsp;&nbsp;<%=I18N.getMsg("mc.components.proxysettings.File")%>&nbsp;:</td>  <%--NO OUTPUTENCODING --%>
        <td><%=dispFileName%> <input type="hidden" name="uploadfile"value="<%=uploadfile%>"> </td><%--NO OUTPUTENCODING --%>
      </tr>
      <input type="hidden" name="todo" value='<%=I18N.getMsg("mc.components.proxysettings.upload")%>'>   <%--NO OUTPUTENCODING --%>
    </table>

    <table border="0" cellpadding="3" cellspacing="0" width="100%">
      <tr align="center">
        <td width="4%">&nbsp;</td>
        <td width="15%" align="right">&nbsp;</td>
        <td width="81%" align="left" valign="middle" nowrap> 
	  <input name="Button" type="submit" style="width:70" class="button" value='<%=I18N.getMsg("mc.components.proxysettings.Send")%>' onClick="this.form.action='ACsupportCreation.ve'">   <%--NO OUTPUTENCODING --%>
          <input name="Submit" type="button" style="width:70" class="button"  value='<%=I18N.getMsg("mc.components.proxysettings.Cancel")%>' onClick="setSendSupport('sendsupportfile','hide');setSendSupport('upbutton','button');setSendSupport('closeview','show');" value='<%=I18N.getMsg("mc.components.proxysettings.Cancel")%>'>  <%--NO OUTPUTENCODING --%>

	  &nbsp; </td>
      </tr>
    </table>
</form>
</div>

<%
if(uploadFileName != null){
%>
<script>
setSendSupport('sendsupportfile','show');
setSendSupport('upbutton','hide');
setSendSupport('closeview','hide');
setSendSupport('closeview','show');

</script>
<%
}
%>

