 <%--$Id$--%>
<%@ include file='/components/jsp/CommonIncludes.jspf'%>
<%@ page import="com.adventnet.i18n.I18N" %>
<%@ page import="com.adventnet.iam.xss.IAMEncoder" %>
<script>parent.includeJS("<%=request.getContextPath()%>/common/js/general.js",window)</script><%--NO OUTPUTENCODING --%>


 <%@ page import="com.adventnet.client.proxysettings.web.*"%> 
 <%@ page import="com.adventnet.client.util.web.*"%>

<%

boolean isDirectConnectionToInternet = Boolean.valueOf((String)request.getAttribute("DIRECT_CONNECTION_TO_INTERNET"));

String proxyHost = getValueFromRequest(request, "PROXY_HOST");

String proxyPort = getValueFromRequest(request, "PROXY_PORT");

String proxyUsername = getValueFromRequest(request, "PROXY_USERNAME");

String proxyPassword = ACClientUtil.getClientProps("PROXY_PASSWORD",true);

proxyPassword = (proxyPassword != null && proxyPassword.length() > 0 ) ? ACClientUtil.NOT_TO_DISPLAY : "";

// Currently, this value is supplied by NCMPatchUpdate.jsp

boolean isPopup = (new Boolean(request.getParameter("IS_POPUP"))).booleanValue();

%>



<%!

private String getValueFromRequest(HttpServletRequest request, String propertyName) throws Exception

{

    // in the table

    // credential keys will be in the column index 2

    // credential value will be in the column index 11



    String value = (String)request.getAttribute(propertyName);

	if(value == null)

	{

		value = ACClientUtil.getClientProps(propertyName);

	}



	return (value != null) ? value: "";

}

%>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" border="0" cellspacing="0" cellpadding="3">

  <tr>

    <td valign="top">

<form name="ProxySettingsForm"> <!-- action="ProxySettings.ve?IS_POPUP=<%=isPopup%>" method="POST" onsubmit="handleStateForForm(this)"> -->

        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="0" class="whitetable">

          <tr>

            <td colspan="3" valign="top"> <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">

                <tr>

                  <td valign="top">

                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">

                      <tr>

                        <td valign="top" width="57%"><table width="100%" border="0" cellspacing="0" cellpadding="3">

                            <tr class="headtxt">

                              <td valign="top"> <input tabindex="1" name="DIRECT_CONNECTION_TO_INTERNET" class="radiobuttonStyle" type="radio" id="radio" value="true" <%=(isDirectConnectionToInternet)?"checked":""%>  onClick="hideElement('proxysettingsrow');">

                                <label class="pointerhand" for="radio"><%=I18N.getMsg("mc.components.proxysettings.Direct_Connection_to_the_Internet")%></label> <br><%--NO OUTPUTENCODING --%>

                                <input tabindex="2" name="DIRECT_CONNECTION_TO_INTERNET" type="radio" class="radiobuttonStyle" id="radio2" value="false" <%=(!isDirectConnectionToInternet)?"checked":""%> onClick="showElement('proxysettingsrow', true);">

                                <label class="pointerhand" for="radio2"><%=I18N.getMsg("mc.components.proxysettings.Use_Proxy_Server_for_the_Internet_connection")%></label><%--NO OUTPUTENCODING --%>

                                <b><a href= "javascript:void(null)" onClick="contextHelp('Proxy_Settings');"></a>

                                </b></td>

                            </tr>

                            <tr id="proxysettingsrow">

                              <td valign="top"> <fieldset>

                                <legend><b><%=I18N.getMsg(" mc.components.proxysettings.Use_Proxy_Server_for_the_Internet_connection")%>&nbsp; </b></legend><%--NO OUTPUTENCODING --%>

                                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                  <tr>

                                    <td valign="top" width="93%"> <table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">

                                        <tr>

                                          <td width="194" align="right"><%=I18N.getMsg("mc.components.proxysettings.HTTP_Proxy_Server")%><span class="redstar">*</span></td><%--NO OUTPUTENCODING --%>

                                          <td width="302" valign="top"> <input tabindex="3" name="PROXY_HOST" type="text" value="<%=IAMEncoder.encodeHTMLAttribute(proxyHost)%>" class="inputStyle" validatemethod="validateEmptyTest" errormsg='<%=I18N.getMsg("mc.components.proxysettings.Please_enter_value_for_proxy_host")%>'><%--NO OUTPUTENCODING --%>

                                          </td>

                                        </tr>

                                        <tr>

                                          <td align="right"><%=I18N.getMsg("mc.components.proxysettings.HTTP_Proxy_Port")%><span class="redstar">*</span><%--NO OUTPUTENCODING --%>

                                          </td>

                                          <td valign="top"> <input tabindex="4" name="PROXY_PORT" type="text" value="<%=IAMEncoder.encodeHTMLAttribute(proxyPort)%>" class="inputStyle"   validatemethod="validatePortTest" errormsg='<%=I18N.getMsg("mc.components.proxysettings.Invalid_value_for_port_Specify_a_valid_value_e_g_161")%>' size="6"><%--NO OUTPUTENCODING --%>

                                          </td>

                                        </tr>

                                        <tr>

                                          <td align="right"><%=I18N.getMsg("mc.components.proxysettings.User_Name")%></td><%--NO OUTPUTENCODING --%>

                                          <td valign="top"> <input tabindex="5" name="PROXY_USERNAME" type="text" value="<%=IAMEncoder.encodeHTMLAttribute(proxyUsername)%>" class="inputStyle">

                                          </td>

                                        </tr>

                                        <tr>

                                          <td align="right" valign="top"><%=I18N.getMsg("mc.components.Password")%></td><%--NO OUTPUTENCODING --%>

                                          <td valign="top"> <p>

                                              <input tabindex="6" name="PROXY_PASSWORD" type="password" value="<%=IAMEncoder.encodeHTMLAttribute(proxyPassword)%>" class="inputStyle">

                                            </p></td>

                                        </tr>

                                      </table>

                                      <br> </td>

                                    <td width="7%" valign="top"> </td>

                                  </tr>

                                </table>

                                </fieldset></td>

                            </tr>

                          </table></td>

                        <td width="43%" valign="top"><br>

                          <table width="90%" border="0" cellpadding="2" cellspacing="0">

                            <tr>

                              <td>&nbsp;</td>

                              <td class="listViewHeader"><%=I18N.getMsg("mc.components.proxysettings.Test_Internet_Connectivity")%></td><%--NO OUTPUTENCODING --%>

                            </tr>

                            <tr>

                              <td width="6%">&nbsp;</td>

                              <td width="94%" class="textSmall"><%=I18N.getMsg("mc.components.proxysettings.Click_here_to_test_the_internet_connectivity_of_the_server")%></td><%--NO OUTPUTENCODING --%>

                            </tr>

                            <tr>

                              <td>&nbsp;</td>

                              <td><input tabindex="8" name="testbutton" type="button" class="button" value='<%=I18N.getMsg("mc.components.proxysettings.Test")%>'onClick="this.form.action='ProxyServerTest.ma';AjaxAPI.submit(this.form);"></td><%--NO OUTPUTENCODING --%>

                            </tr>

                          </table></td>

                      </tr>

                      <tr>

                        <td colspan="2" valign="top"> </td>

                      </tr>

                      <br>

                      <br>

                      <tr>

                        <td colspan="2" valign="top"></td>

                      </tr>

                    </table></td>

                </tr>

                <tr>

                  <td width="100%" valign="top"> </td>

                </tr>



              </table></td>

          </tr>

          <tr class="tableheaderConfig">

            <td width="20%">&nbsp;</td>

            <td width="44%"> <input tabindex="7" name="Submit" type="button" class="button" value='<%=I18N.getMsg("mc.components.proxysettings.Save_Settings")%>' onClick="this.form.action='<%=IAMEncoder.encodeJavaScript(uniqueId)%>.ve';AjaxAPI.submit(this.form);"><%--NO OUTPUTENCODING --%>

            </td>

            <td width="36%">&nbsp;</td>

          </tr>

        </table>

        <script>

if(<%=!(isDirectConnectionToInternet)%>)

{

	showElement('proxysettingsrow');

}

else

{

	hideElement('proxysettingsrow');

}

function validatePortTest(value,formElement)

{ 

 if(document.ProxySettingsForm.DIRECT_CONNECTION_TO_INTERNET[1].checked)

  {

     return isPositiveInteger(document.ProxySettingsForm.PROXY_PORT.value);

  }

  else

   {

       return true;

   }

}

 function showElement(id)
{
  document.getElementById(id).style.display = "";
}

function hideElement(id)
{
  document.getElementById(id).style.display =  'none';
}



function validateEmptyTest(value,formElement)

{

     if(document.ProxySettingsForm.DIRECT_CONNECTION_TO_INTERNET[1].checked)

    {

        return isNotEmpty(document.ProxySettingsForm.PROXY_HOST.value);

    }

    else

    {

        return true;

    }

}



</script>

      </form></td>

  </tr>

</table>

<br>

