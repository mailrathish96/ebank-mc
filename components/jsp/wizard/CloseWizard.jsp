<%-- $Id$ --%>
<%@page import="com.adventnet.iam.xss.IAMEncoder"%>
<script src='<%=request.getContextPath()%>/components/javascript/Utils.js'></script><%--NO OUTPUTENCODING --%>
<script>
var WIZARD_FINISH=true;
var paramsToPass = '<%=IAMEncoder.encodeJavaScript((String)request.getAttribute("ADDITIONAL_PARAMS"))%>';

if("<%=IAMEncoder.encodeJavaScript(request.getParameter("WINDOWTYPE"))%>" == "STANDALONE")
{
   reloadAndCloseWindow(paramsToPass);
}
else
{
   popContentArea(-1,null);
}
</script>