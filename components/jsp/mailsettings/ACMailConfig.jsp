<%--$Id$--%>
<%@ include file='/components/jsp/CommonIncludes.jspf'%>
<%@ page import="com.adventnet.iam.xss.IAMEncoder" %>
<script src='<%=request.getContextPath()%>/components/javascript/ACmailConfig.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script src='<%=request.getContextPath()%>/components/javascript/cv_creation/general.js' type='text/javascript'></script><%--NO OUTPUTENCODING --%>
<script>
function test(fm)
{
var validatemethod=document.getElementById("FROM_ADDRESS").getAttribute("validatemethod");
document.getElementById("FROM_ADDRESS").removeAttribute("validatemethod");
if(validateForm(fm))
{
fm.action='MailServerTest.ma';
AjaxAPI.submit(fm);
}
document.getElementById("FROM_ADDRESS").setAttribute("validatemethod",validatemethod);
}
function save(form)
{
if(validateForm(form))
{
 populateOptions(form.EMAIL_SELECT_VALUES,form.EMAIL_SELECT);
} 
 else{return false} 
 form.action='<%=IAMEncoder.encodeJavaScript(uniqueId)%>.ve';
 AjaxAPI.submit(form);
}

function check(el)
{
 
  document.getElementById("MAIL_ACCOUNT_PASSWORD5").setAttribute("validatemethod","isNotEmpty");
  document.getElementById("MAIL_ACCOUNT_PASSWORD5").setAttribute("errormsg","Enter password");
  document.getElementById("MAIL_ACCOUNT_NAME5").setAttribute("validatemethod","isNotEmpty");
  document.getElementById("MAIL_ACCOUNT_NAME5").setAttribute("errormsg","Enter username");
if(el.checked )
{
showHideComponent('user_info',true)
}
if(!el.checked)
{
showHideComponent('user_info',false)
}
}


</script>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.adventnet.persistence.*" %>
<%@ page import="com.adventnet.client.util.web.*"%>
<%@ page import="java.util.*" %>
<%@ page import="com.adventnet.i18n.I18N" %>




<%
    // Mail Server
    HashMap mail_server = (HashMap) request.getAttribute("MAIL_SERVER_SETTINGS");
    String server_name = (String) mail_server.get("SERVER_NAME");
    String server_port = ((Integer) mail_server.get("SERVER_PORT")).toString();
    String server_acname = (String) mail_server.get("ACCOUNT_NAME");
    String server_acpass = (String) mail_server.get("ACCOUNT_PASSWORD");
    server_acname = (server_acname != null) ? server_acname : "";
    server_acpass = (server_acpass != null) ? server_acpass : "";
    boolean isAuth = (server_acname != "" && server_acpass != "");

    List email_list = (List) request.getAttribute("EMAIL_LIST");

    String defaultEmail = ACClientUtil.getDefaultFromAddress();

  if(defaultEmail == null)   {
        defaultEmail = "";
 }


   %>
   <script>
  parent.emailNames = new Array();
   </script>
   


<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="whitetable">
  <tr>
    <td valign="top">
      <form name="ServerSettingsView"   >
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="header"><b><%=I18N.getMsg("mc.components.mailsettings.Mail_Server_Settings")%>&nbsp;</b><a href= "javascript:void(null)" onClick="contextHelp('Mail_settings');"></td><%--NO OUTPUTENCODING --%>
          </tr>
        </table>
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="whitetable">
          <tr>
            <td width="53%" valign="top"> <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="whitetable" id="mailserversettings_view">
                <tbody>
                  <tr>
                    <td width="35%" align="right" valign="top">&nbsp;</td>
                    <td align="left" valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="right" valign="top"><%=I18N.getMsg("mc.components.mailsettings.SMTP_Server")%><span class="mandatory">*</span>&nbsp;</td><%--NO OUTPUTENCODING --%>
                    <td width="342" align="left" valign="top"> <label>
                      <input name="MAIL_SERVER_NAME" type="text" class="inputStyle" id="MAIL_SERVER_NAME3" size="25" value="<%=IAMEncoder.encodeHTMLAttribute(server_name)%>" validatemethod="isNotEmpty" errormsg='<%=I18N.getMsg("mc.components.mailsettings.Enter_SMTP_server_name")%>'> <%--NO OUTPUTENCODING --%>
                      </label><div id="MAIL_SERVER_NAME3_DIV"></div></td>
                  </tr>
                  <tr>
                    <td align="right" valign="top"><%=I18N.getMsg("mc.components.mailsettings.Port")%><span class="mandatory">*</span>&nbsp;</td><%--NO OUTPUTENCODING --%>
                    <td align="left" valign="top"> <input name="MAIL_SERVER_PORT" type="text" class="inputStyle"
 id="MAIL_SERVER_PORT3" size="6" maxlength="25" value="<%=server_port%>" validatemethod="isNumeric" errormsg='<%=I18N.getMsg("mc.components.mailsettings.Enter_valid_port_number")%>'><div id="MAIL_SERVER_PORT3_DIV"></div><%--NO OUTPUTENCODING --%>
                    </td>
                  </tr>
                  <tr>
                    <td align="right" valign="top">&nbsp;</td>
		                        <td valign="top"><input name="CHECKBOX" class="checkboxStyle" type="checkbox" id="checkbox1" value="true" <%=(isAuth == true)? "checked" : ""%> onClick="return check(this)" >
                      <label class="pointerhand" for="checkbox1"><span class="textLined"><%=I18N.getMsg("mc.components.mailsettings.Authenticate_for_every_Login")%></span></label></td><%--NO OUTPUTENCODING --%>
                  </tr>
                  <tr>
                    <td colspan="2" valign="top" class='<%=(isAuth == true)? "tablerowslight" : "hide"%> ' id="user_info">
                      <table class="whitetable" cellspacing=0 cellpadding=2 width="100%"
                              align=center border=0>
                        <tbody>
                          <tr>
                            <td width="35%" align="right" valign="top"><%=I18N.getMsg("mc.components.mailsettings.Username")%></td><%--NO OUTPUTENCODING --%>
                            <td width="324" align="left" valign="top"> <input name="MAIL_ACCOUNT_NAME" type="text" class="inputStyle" id="MAIL_ACCOUNT_NAME5"  size="25" value="<%=IAMEncoder.encodeHTMLAttribute(server_acname)%>"><div id="MAIL_ACCOUNT_NAME5_DIV"></div>

                            </td>
                          </tr>
                          <tr>
                            <td align="right" valign="top"><%=I18N.getMsg("mc.components.Password")%></td><%--NO OUTPUTENCODING --%>
                            <td align="left" valign="top"> <input name="MAIL_ACCOUNT_PASSWORD" type="password" class="inputStyle" id="MAIL_ACCOUNT_PASSWORD5" size="25"  value="<%=IAMEncoder.encodeHTMLAttribute(server_acpass)%>"><div id="MAIL_ACCOUNT_PASSWORD5_DIV"></div>
                            </td>
                          </tr>
                        </tbody>
                      </table></td>
                  </tr>
                  <tr>
                    <td align="right"></td>
                   </tr>
                </tbody>
              </table></td>
            <td align="center" valign="top"><br>
              <table width="80%" border="0" cellpadding="2" cellspacing="0">
                <tr>
                  <td>&nbsp;</td>
                  <td class="textBig"><%=I18N.getMsg("mc.components.mailsettings.Test_Mail_Server")%></td><%--NO OUTPUTENCODING --%>
                </tr>
                <tr>
                  <td width="6%">&nbsp;</td>
                  <td width="94%"><%=I18N.getMsg("mc.components.mailsettings.Click_here_to_test_the_mail_server_configuration")%></td><%--NO OUTPUTENCODING --%>
                </tr>
                <tr>
                  <td>&nbsp;</td>
             <td><input name="testbutton" type="button" class="button" value=">> Test" onClick="return test(this.form)"></td>
              </tr>
              </table> </td>
          </tr>
        </table>
        <br>
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
          <tr>
            <td class="header" valign="top"> <b><%=I18N.getMsg("mc.components.mailsettings.Email_Notification_Settings")%>&nbsp;</b></td><%--NO OUTPUTENCODING --%>
          </tr>
        </table>
        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="0" class="whitetable">
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
            <td class="helpcardtxt">&nbsp;</td>
          </tr>
          <tr>
            <td width="18%" align="right" valign="top"><%=I18N.getMsg("mc.components.mailsettings.From")%></td><%--NO OUTPUTENCODING --%>
	
            <td width="505" valign="top"> <input name="FROM_ADDRESS" id="FROM_ADDRESS" type="text" class="inputStyle" value="<%=IAMEncoder.encodeHTMLAttribute(defaultEmail)%>" size="40" validatemethod="isEmailId" errormsg='<%=I18N.getMsg("mc.components.mailsettings.Enter_valid_from_email_address")%>'><%--NO OUTPUTENCODING --%>
              <div id="FROM_ADDRESS_DIV"></div><br> </td>
            <td class="helpcardtxt" width="287">&nbsp;</td>
          </tr>
          <tr>
            <td align="right" valign="top"><%=I18N.getMsg(" mc.components.mailsettings.Notify_To")%><br> </td><%--NO OUTPUTENCODING --%>
            <td valign="top"> <table width="100%" border="0" cellspacing="0" cellpadding="1" class="whitetable">
                <tr>
                  <td width="23%"><input name="EMAIL_ADDRESS"  type="text" class="inputStyle" size="40" maxlength="65" id="notifyAddress"  errormsg='<%=I18N.getMsg("mc.components.mailsettings.Enter_valid_email_address")%>'><div id="EMAIL_ADDRESS_DIV"></div></td><%--NO OUTPUTENCODING --%>
                  <td width="77%"><input name="add" type="button" class="button" id="add5" value='<%=I18N.getMsg("mc.components.mailsettings.Add")%>'<%--NO OUTPUTENCODING --%>
            onClick="if(validateElement('isEmailId',this.form.notifyAddress))
            {
                           if(validateMailForDuplication(EMAIL_ADDRESS.value,this.form.EMAIL_SELECT))
                             {
                                addValueInSelectComp(EMAIL_ADDRESS.value,this.form.EMAIL_SELECT);
								var a = document.getElementById('notifyAddress');
								a.value='';
                            }
                      }"></td>
                </tr>
                <tr>
                  <td valign="top"> <select name="EMAIL_SELECT" size="5"   class="listStyle" style="height:100px;border:1px solid #000000;">
                      <%
           for(int i=0;i<email_list.size();i++)
             {
               String email_id = (String) email_list.get(i);

         %>
                      <option value="<%=email_id%>"><%=email_id%></option>
                      <%
            }
        %>
                    </select> </td>
                  <td valign="top"> <br> <br> <input name="remove" type="button" class="button" id="remove4" value='<%=I18N.getMsg("mc.components.mailsettings.Remove")%>' onClick="validateUsedValues(this.form.EMAIL_SELECT)" ></td><%--NO OUTPUTENCODING --%>
                </tr>
              </table></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td width="505" valign="top">&nbsp; </td>
            <td width="287">&nbsp; </td>
          </tr>
          <tr class="tableheaderConfig">
            <td align="middle">&nbsp;</td>
            <td> <input name="button" class="button" type="submit" onClick="return save(this.form)" value='<%=I18N.getMsg("mc.components.mailsettings.Save_Settings")%>' ></td><%--NO OUTPUTENCODING --%>
          </tr>
        </table>

      <select name="EMAIL_SELECT_VALUES" id="EMAIL_SELECT_VALUES" multiple size="10" class="hide"></select>
      </form></td>
  </tr>
</table>
<SCRIPT>
    var usedEmailAddresses = '<%=IAMEncoder.encodeJavaScript((String)request.getAttribute("USED_EMAIL_ADDRESSES"))%>';
    if( usedEmailAddresses != 'null' )
    {
        alert("The following addresses cannot be removed as they are used in schedules.'\n\t'"+
                usedEmailAddresses);
    }
</SCRIPT>
