<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="styles.css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>

<html>
<head>
    <title>E-Banking</title>
</head>
<body>
<%
    String ben_id = request.getParameter("ben_id");
%>
<script type="text/javascript">
    (function($) {
		$.ajax({
        type: "post",
        url: "BeneficiaryAction.do?method=getBeneficiaryDetails",
        data: "ben_id=<%=ben_id%>",
        success: function(msg){
			console.log(msg);
            document.getElementById("ben-accno").value = msg.acc_no;
			document.getElementById("ben-name").value = msg.name;
            document.getElementById("ben-bank").value = msg.bank;
            document.getElementById("ben-branch").value = msg.branch;
            document.getElementById("ben-ifsc").value = msg.ifsc;
        },
        error : function(errorThrown) {
            alert(errorThrown);
        }
    });
	})(jQuery);
</script>
<script type="text/javascript">
    (function ($) {
    $(document).ready(function() {
        $('#edit-submit').click(function () {

                $.ajax({
                    type: "post",
                    url: "BeneficiaryAction.do?method=editOtherbankBenefeciary",
                    data: "ben_id=<%=ben_id%>&acc_no="+$('#ben-accno').val()+"&name="+$('#ben-name').val()+"&bank_name="
                    +$('#ben-bank').val()+"&branch_name="+$('#ben-branch').val()+"&ifsc="+$('#ben-ifsc').val(),
                    success: function(msg){
						modifySystem();
                    },
                    error : function(errorThrown) {
                        alert(errorThrown);
                    }
                });
        });
    });
	})(jQuery);
</script>
    <center><h3>Edit Beneficiary Details</h3></center>
	    
    <div id="otherbank-block" style="display:block; width:75%;">
        <div id="otherbank">
            <p>
               <label class="label-text">Account number</label>
               <input type ="text" disabled="disabled" class="text-box" id="ben-accno" />
            </p>
            <p>
               <label class="label-text">Name</label>
               <input type ="text" class="text-box" id="ben-name" />
            </p>
            <p>
               <label class="label-text">Bank</label>
               <input type ="text" class="text-box" id="ben-bank" />
            </p>
            <p>
               <label class="label-text">Branch</label>
               <input type ="text" class="text-box" id="ben-branch" />
            </p>
            <p>
               <label class="label-text">IFSC</label>
               <input type ="text" class="text-box" id="ben-ifsc" />
            </p>
            <p id = "add-otherbank-beneficiary-message"></p>
            <input type="submit" class="submit-button" id="edit-submit" value="Edit beneficiary"/>
        </div>
        <div class="successfull-entry" id="otherbank-beneficiary-success">
            <p class="success-message"><b>Beneficiary editted successfully!!!</b></p>
        </div>
    </div>    

</body>
</html>