<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.adventnet.com/webclient/clientframework" prefix="client"%>
<%@ page import="com.adventnet.i18n.I18N"%>
<link rel="stylesheet" type="text/css" href="${dmcssUrl}/themes/styles/<c:out value='${selectedskin}'/>/style.css?mdmga3">
<link rel="stylesheet" type="text/css" href="${dmcssUrl}/themes/styles/<c:out value='${selectedskin}'/>/usersearch.css">
<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="${dmcssUrl}/css/genie.css" />
<html>
    <head>
	
	  <link rel="stylesheet" href="/themes/styles/box.css">
	  <link rel="stylesheet" href="/themes/styles/common.css">
	  <link rel="stylesheet" href="/themes/styles/navigation.css">
	  <link rel="stylesheet" href="/themes/styles/searchrow.css">
	  <link rel="stylesheet" href="/themes/styles/dm-default/layout.css">
	  <link rel="stylesheet" href="/themes/styles/dm-default/menu.css">
	  <link rel="stylesheet" href="/themes/styles/dm-default/stylesheet.css">



    <br><br>
	<div class="container-fluid" >
        <client:showView viewName="MenuInTable"/>
    </div>
	

<div id="myModal" class="modal">

  <div class="modal-content">
    <span class="close">&times;</span>
    <p>Some text in the Modal..</p>
  </div>

</div>

</html>