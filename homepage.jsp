
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
	<link rel="stylesheet" type="text/css" href="styles.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title><tiles:getAsString name="title" ignore="true" /></title>
    </head>
    <body>
	    <div class="homepage">
		    <div class="header">
			<tiles:insert attribute="header" ignore="true" />
			</div>
			<div class="body">
			     <div class="menu">
				 <tiles:insert attribute="menubar" />
				 </div>
				 <div class="content">
				 <tiles:insert attribute="content" />
				 </div>
			</div>
		</div>
    </body>
</html>
